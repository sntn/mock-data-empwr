// importing components from react-router-dom package
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { RecoilRoot } from "recoil";

import Dashboard from "./views/Dashboard/Dashboard";
// import MemberTab from "./views/Member/Member";
import Login from "./views/Login/Login";
import Splash from "./components/splash/Splash";
import { useAuth0 } from "@auth0/auth0-react";
import history from "./utils/history";
import AppLayout from "./components/layout/layout";
import "boxicons/css/boxicons.min.css";
import Signup from "./views/Signup/Signup";
import Members from "./views/Members/Members";
import AllAssessmentQuestionsPage from "./views/AssessmentPdf/AllAssessmentQuestionsPage";
import UserAssessmentQuestionsPage from "./views/AssessmentPdf/UserAssessmentQuestionsPage";
import NewDashboard from "./views/Details/NewDashboard";
import NotFoundPage from "./views/NotFoundPage";
import PeriodicAssessmentPage from "./views/AssessmentPdf/PeriodicAssessmentPage";

function App() {
  // const { isLoading, error } = useAuth0();

  // if (error) {
  //   return <div>Oops... {error.message}</div>;
  // }

  // if (isLoading) {
  //   return <Splash />;
  // }

  return (
    <>
      <RecoilRoot>
        <Router history={history}>
          <Routes>
            <Route path="/" element={<Login />} />
            <Route path="/dashboard" element={<AppLayout />}>
              <Route index element={<Dashboard />} />
              <Route path="/dashboard/detailed" element={<NewDashboard />} />
              <Route path="/dashboard/signup" element={<Signup />} />
              <Route path="/dashboard/members" element={<Members />} />
            </Route>
            <Route
              path="/assessment/all"
              element={<AllAssessmentQuestionsPage />}
            />
            <Route
              exact
              path="/assessment/:token"
              element={<UserAssessmentQuestionsPage />}
            />
            <Route
              exact
              path="/assessment/periodic/:user/:assessmentVersion"
              element={<PeriodicAssessmentPage />}
            />
            <Route path="*" element={<NotFoundPage />} />
          </Routes>
        </Router>
      </RecoilRoot>
    </>
  );
}

export default App;
