import React, { useEffect, useState } from "react";
import Alert from "@mui/material/Alert";
import Snackbar from "@mui/material/Snackbar";
import Loading from "../../components/loading/Loading";
import "./Signup.css";
import { useAuth0, withAuthenticationRequired } from "@auth0/auth0-react";
import getUserData from "../../utils/localstorage";

function Signup() {
  /**
   * userData:Object - stores data about the current user.
   * email, firstName and lastName - stores the email, firstname and lastname of the user respectively.
   * error:String - used to show error message to the user while filling up the form.
   * showAlert and alertType - used to show an alert to the user on successful/unsuccessful submission of the form.
   * showAlert:Boolean - controls the alert visibility.
   * alertType:String - 'success' on successful sign up || 'error' on server error
   */
  const [userData, setUserData] = useState(undefined);
  const [email, setEmail] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [phone, setPhone] = useState("");
  const [error, setError] = useState("");
  const [showAlert, setShowAlert] = useState(false);
  const [alertType, setAlertType] = useState("success");

  const { user, getAccessTokenSilently } = useAuth0();

  /**getAccessTokenSilently - returns a Promise that resolves to an access token
   * that can be used to make a call to a protected API.
   * to know more about these functions please visit
   * https://auth0.com/docs/quickstart/spa/react/02-calling-an-api
   */
  useEffect(() => {
    const getData = async () => {
      try {
        // const token = await getAccessTokenSilently({
        //   audience: "https://quicksight/",
        // });
        const token = "sampleToken"

        const tempUserData = await getUserData(/* user.sub, token */);
        setUserData(tempUserData);
      } catch (e) {
        console.error(e);
      }
    };

    getData();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();

    /**If the required fields(i.e. firstName, lastName and email) are not provided,
     * cancel submission and show an error message.
     */
    if (!firstName || !lastName || !email || !phone) {
      setError("Please enter all fields");
      return;
    } else if (
      //If the email provided is invalid, cancel submission and show an error message.
      !/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
        email
      )
    ) {
      setError("Please enter valid email");
      return;
    }

    let cleanPhn = phone.replace(/[-\s()]/g, "");
    console.log({ cleanPhn });
    if (!cleanPhn.startsWith("+1")) {
      setError("country code(+1) missing in entered phone no.");
      return;
    }
    let stdFormat = cleanPhn.substring(2);
    if (stdFormat.length !== 10) {
      setError("10 digit phone no. expected");
      return;
    }
    console.log({ stdFormat });
    if (!/^[0-9]+$/.test(stdFormat)) {
      setError("only digits expected in phone no");
      return;
    }

    //If all the required fields are provided correctly, the error message is removed on subsequent submission.
    setError("");

    //Creating the payload that will be sent to the api
    const data = { email, firstName, lastName };

    //Clearing up the fields.
    setEmail("");
    setFirstName("");
    setLastName("");
    setPhone("");

    const baseurl =
      !process.env.NODE_ENV || process.env.NODE_ENV === "development"
        ? "http://localhost:5000"
        : "";

    try {
      // const token = await getAccessTokenSilently({
      //   audience: "https://quicksight/",
      // });
      const token = "sampleToken";

      const requestOptions = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(data),
      };

      const response = await fetch(
        `${baseurl}/api/patient/signup`,
        requestOptions
      );

      if (response.status === 200) setAlertType("success");
      else setAlertType("error");
      setShowAlert(true);
    } catch (e) {
      console.error(e.message);
    }
  };

  return (
    <div className="signup-container">
      {/**This is the alert section, that shows up when user makes sign up request */}
      <Snackbar
        open={showAlert}
        autoHideDuration={3000}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        onClose={() => setShowAlert(false)}
        sx={{ paddingLeft: 40, textAlign: "center" }}
      >
        <Alert onClose={() => setShowAlert(false)} severity={alertType}>
          {alertType === "success"
            ? "User signed up successfully."
            : "Error occurred while sign up. Please try again later."}
        </Alert>
      </Snackbar>
      <form className="signup-form" onSubmit={handleSubmit}>
        {userData && userData.logo && (
          <img
            className="signup-form-logo"
            src="/logos/wellness.png"
            height="50px"
            alt={`${userData.client} Logo`}
          />
        )}
        <label>Firstname</label>
        <input
          className="signup-input"
          type="text"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        />
        <label>Lastname</label>
        <input
          className="signup-input"
          type="text"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        />
        <label>Email</label>
        <input
          className="signup-input"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <label>
          Phone&nbsp;
          <span style={{ fontWeight: "normal" }}>
            (please enter in international format. i.e. +1-[10 digits])
          </span>
        </label>
        <input
          className="signup-input"
          value={phone}
          onChange={(e) => setPhone(e.target.value)}
        />
        {error && <span className="errorMsg">{error}</span>}
        <button className="signup-btn" type="submit">
          Sign Up
        </button>
      </form>
    </div>
  );
}

/**
 * withAuthenticationRequired is used to make the page protected
 * users will be redirected to the login page if not authorized.
 */
export default Signup;
