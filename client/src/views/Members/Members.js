import Loading from "../../components/loading/Loading";
import { Nodata } from "../../icons/dashboard";
import {
  Email,
  Google,
  Facebook,
  Copy,
  Send,
  NoAccount,
} from "../../icons/signup";
import getUserData from "../../utils/localstorage";
import SearchBar from "../../components/searchbar/SearchBar";

import { useAuth0, withAuthenticationRequired } from "@auth0/auth0-react";
import React, { useEffect, useState, useMemo } from "react";
import { useTable, useSortBy, usePagination } from "react-table";
import Alert from "@mui/material/Alert";
import Snackbar from "@mui/material/Snackbar";
import Modal from "react-modal";
import { Oval } from "react-loader-spinner";

function Members() {
  // const { user, getAccessTokenSilently } = useAuth0();

  const [userData, setUserData] = useState(undefined);
  const [filteredData, setFilteredData] = useState(undefined);
  const [memberData, setMemberData] = useState(undefined);
  const [searchText, setSearchText] = useState("");
  const [showAlert, setShowAlert] = useState(false);
  const [alertType, setAlertType] = useState("success");

  const day = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];

  const month = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  var today = new Date();

  /** This function returns the suffix of the date
   * e.g. if the date is 23rd, it'll return "rd"
   * or if it is 12th, it'll return "th".
   */
  function dateSuffix() {
    var last = today.getDate() % 10;
    return last == 1 ? "st" : last == 2 ? "nd" : last == 3 ? "rd" : "th";
  }

  const baseurl =
    !process.env.NODE_ENV || process.env.NODE_ENV === "development"
      ? "http://localhost:5000"
      : "";

  const rowOpertation = (row) => {
    row.name = row.user.given_name + " " + row.user.family_name;
    row.email = row.user.email;
    row.inviteStatus = row.active ? "Unused" : "Used";
    row.accountCreated = row.user.auth0Id ? "Yes" : "No";
    row.id = row.user["_id"];
    if (row.user.auth0Id) {
      switch (row.user.auth0Id.split("|")[0]) {
        case "google-oauth2":
          row.signupMethod = "google";
          break;
        case "facebook":
          row.signupMethod = "facebook";
          break;
        default:
          row.signupMethod = "email";
      }
    }
    row.assessmentCompleted = row.user.lastAssessmentDate ? "Yes" : "No";
  };

  useEffect(() => {
    /**This function fetches data for all three ranges
     * and stores it in the data variable.
     */
    const getData = async () => {
      try {
        // const token = await getAccessTokenSilently({
        //   audience: "https://quicksight/",
        // });
        const token = "sampleToken";

        const tempUserData = await getUserData(/* user.sub, token */);

        const response = await fetch(`${baseurl}/api/patient/status`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        const result = await response.json();
        //result.data.forEach((row) => rowOpertation(row));

        setUserData(tempUserData);
        setMemberData(result.data);
        setFilteredData(result.data);
      } catch (e) {
        console.error(e);
      }
    };

    getData();
  }, []);

  useEffect(() => {
    if (memberData) {
      const filter = memberData.filter((item) => {
        if (searchText === "") {
          return true;
        } else {
          return item.name.toLowerCase().includes(searchText);
        }
      });
      setFilteredData(filter);
    }
  }, [searchText]);

  if (!filteredData) return <Loading />;
  return (
    <div id="dashboard">
      <Snackbar
        open={showAlert}
        autoHideDuration={3000}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        onClose={() => setShowAlert(false)}
        sx={{ paddingLeft: 40, textAlign: "center" }}
      >
        <Alert onClose={() => setShowAlert(false)} severity={alertType}>
          {alertType === "success"
            ? "Invite code sent to user."
            : "Error occurred while sending the invite code. Please try again later."}
        </Alert>
      </Snackbar>
      {/**Header section to show the username and the current date with searchbar */}
      <div className="client_logo">
        {userData && userData.logo && (
          <img
            src={userData.logo}
            height="50px"
            alt={`${userData.client} Logo`}
          />
        )}
      </div>
      <div className="heading">
        <p>Hi, {userData.name}</p>

        <p>
          {day[today.getDay()] +
            ", " +
            month[today.getMonth()] +
            " " +
            today.getDate() +
            dateSuffix() +
            ", " +
            today.getFullYear()}
        </p>
      </div>
      <SearchBar search={searchText} setSearchText={setSearchText} />
      <StatusTable
        data={filteredData}
        setAlertType={setAlertType}
        setShowAlert={setShowAlert}
      />
    </div>
  );
}

function StatusTable(props) {
  const data = useMemo(() => props.data, [props.data]);

  const [modalOpen, setModalOpen] = useState(false);

  const handleModalOpen = () => setModalOpen(true);
  const handleModalClose = () => setModalOpen(false);

  const { getAccessTokenSilently } = useAuth0();

  Modal.setAppElement("#root");

  const customModalStyles = {
    overlay: {
      backgroundColor: "rgba(0, 0, 0, 0.3)",
    },
    content: {
      borderRadius: "8px",
      padding: "8px 15px",
      backgroundColor: "#086972",
      color: "white",
      top: "80px",
      left: "calc(50% + 160px)",
      right: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      height: "30px",
    },
  };

  const signupIconMap = {
    email: Email,
    google: Google,
    facebook: Facebook,
  };

  const baseurl =
    !process.env.NODE_ENV || process.env.NODE_ENV === "development"
      ? "http://localhost:5000"
      : "";

  const sendinvite = async (row) => {
    handleModalOpen();
    try {
      const token = await getAccessTokenSilently({
        audience: "https://quicksight/",
      });

      const options = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(
          row.inviteStatus == "Unused"
            ? {
                name: row.name,
                email: row.email,
                code: row.code,
              }
            : {
                user: row.id,
              }
        ),
      };

      const response = await fetch(`${baseurl}/api/patient/invite`, options);
      const result = await response.json();
      handleModalClose();
      if (response.status === 200) {
        props.setAlertType("success");
        if (result.reload)
          setTimeout(() => window.location.reload(false), 2000);
      } else props.setAlertType("error");
      props.setShowAlert(true);
    } catch (e) {
      console.error(e);
    }
  };

  const columns = useMemo(
    () => [
      {
        Header: "Signup Method",
        accessor: "signupMethod",
        tiptext: "The method used for creating the account",
        maxWidth: 50,
        Cell: (props) =>
          props.value ? (
            <img src={signupIconMap[props.value]} width="24px" height="24px" />
          ) : (
            <img src={NoAccount} width="24px" height="24px" />
          ),
      },
      {
        Header: "Name",
        accessor: "name",
        tiptext: "Name of the patient",
        Cell: (props) => (
          <span>
            {props.value}
            <img
              src={Copy}
              style={{ paddingLeft: 3, verticalAlign: "bottom" }}
              width="16px"
              height="16px"
              onClick={() => copy(props.value)}
              title="Copy"
            />
          </span>
        ),
      },
      {
        Header: "Email",
        accessor: "email",
        tiptext: "Email of the patient",
        Cell: (props) => (
          <span>
            {props.value}
            <img
              src={Copy}
              style={{ paddingLeft: 3, verticalAlign: "bottom" }}
              width="16px"
              height="16px"
              onClick={() => copy(props.value)}
              title="Copy"
            />
          </span>
        ),
      },
      {
        Header: "Code",
        accessor: "code",
        tiptext: "Last generated invite code",
        Cell: (props) => (
          <span>
            {props.value}
            <img
              src={Copy}
              style={{ paddingLeft: 3, verticalAlign: "bottom" }}
              width="16px"
              height="16px"
              onClick={() => copy(props.value)}
              title="Copy"
            />
          </span>
        ),
      },
      {
        Header: "Invite Status",
        accessor: "inviteStatus",
        tiptext: "Status of the invite code",
        maxWidth: 60,
      },
      {
        Header: "Account Created",
        accessor: "accountCreated",
        tiptext: "Account Creation Status",
        maxWidth: 60,
      },

      {
        Header: "Assessment Completed",
        accessor: "assessmentCompleted",
        tiptext: "Status of assessment",
        maxWidth: 60,
      },
      {
        Header: "",
        accessor: "id",
        width: 30,
        Cell: (props) => (
          <img
            src={Send}
            width="20px"
            height="20px"
            onClick={() => sendinvite(props.row.values)}
            title="Send Invite"
          />
        ),
      },
    ],
    []
  );

  function copy(value) {
    navigator.clipboard.writeText(value);
  }

  //Set of colors used in the table.
  const color = {
    Yes: "#086972",
    No: "#FF2727",
    Unused: "#086972",
    Used: "#FF2727",
  };

  const getColor = (id, index) => {
    if (id == "inviteStatus") {
      return color[data[index].inviteStatus];
    } else if (id == "accountCreated") {
      return color[data[index].accountCreated];
    } else if (id == "assessmentCompleted") {
      return color[data[index].assessmentCompleted];
    } else return null;
  };

  // Creates an instance of the table.
  const tableInstance = useTable(
    { columns, data: data, initialState: { pageIndex: 0 } },
    useSortBy,
    usePagination
  );

  /**
   * headerGroups -> An array of object.
   * It containins a flattened array of final column objects for that row.
   * It has a header field this header field is used to map the colums of the table.
   * headerGroups.header = [{
   *  Header, //the header name
   *  tiptext //the text that will be shown once we hover over the header
   * }]
   *
   * page -> An array of rows for the current page, determined by the current pageIndex value.
   *
   * //pageination variables
   * The dataset is divided into pages.
   *
   * pageSize -> number of records in each page, 10 by default
   * setPageSize -> function to change the no of records in each page.
   * Takes an integer argument.
   * pageCount -> returns total number of pages.
   * pageIndex -> returns the current page's index, 0 by default
   * gotoPage -> function to move between pages. Takes an integer argument - the page number.
   * canPreviousPage -> boolean variable. Returns false if there is no previous page.
   * canNextPage -> boolean variable. Returns false if there is no next page.
   * pageOptions -> array of zero-based index integers corresponding to available pages in the table.
   *
   * For more information, check out the documentation of react-table at https://react-table-v7.tanstack.com/
   */
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = tableInstance;

  if (data.length == 0)
    return (
      <div className="nodata">
        <img src={Nodata} width="80px" height="80px" />
        <p>No data available</p>
      </div>
    );

  return (
    <div>
      <Modal
        isOpen={modalOpen}
        style={customModalStyles}
        contentLabel="Sending email"
      >
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <span
            style={{
              paddingRight: "30px",
            }}
          >
            Sending email to the user.
          </span>
          <Oval
            height={20}
            width={20}
            color="white"
            wrapperStyle={{}}
            wrapperClass=""
            visible={true}
            ariaLabel="oval-loading"
            secondaryColor="white"
            strokeWidth={4}
            strokeWidthSecondary={4}
          />
        </div>
      </Modal>
      <div className="wrapper">
        <table className="table" {...getTableProps()}>
          <thead>
            {headerGroups.map((headerGroup) => (
              <tr className="row header" {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column, index) => (
                  <th
                    className="cell new-tooltip"
                    {...column.getHeaderProps({
                      ...column.getSortByToggleProps(),
                      style: { maxWidth: column.maxWidth },
                    })}
                  >
                    {column.render("Header")}
                    {headerGroup.headers[index].tiptext && (
                      <span className="tooltip">
                        {headerGroup.headers[index].tiptext}
                      </span>
                    )}
                    <span className="sort-options">
                      {column.isSorted
                        ? column.isSortedDesc
                          ? " 🔽"
                          : " 🔼"
                        : ""}
                    </span>
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {page.map((row) => {
              prepareRow(row);
              return (
                <tr className="row" {...row.getRowProps()}>
                  {row.cells.map((cell) => {
                    return (
                      <td
                        className="cell"
                        {...cell.getCellProps({
                          style: {
                            color: getColor(cell.column.id, row.index),
                            fontWeight:
                              cell.column.id == "inviteStatus" ||
                              cell.column.id == "accountCreated" ||
                              cell.column.id == "assessmentCompleted"
                                ? 600
                                : null,
                          },
                        })}
                      >
                        {cell.render("Cell")}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      {/**bottom pagination section */}
      <div className="pagination">
        <div className="controls">
          <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
            {"<<"}
          </button>{" "}
          <button onClick={() => previousPage()} disabled={!canPreviousPage}>
            {"<"}
          </button>{" "}
          <span>
            Page{" "}
            <strong>
              {pageIndex + 1} of {pageOptions.length}
            </strong>{" "}
          </span>
          <button onClick={() => nextPage()} disabled={!canNextPage}>
            {">"}
          </button>{" "}
          <button
            onClick={() => gotoPage(pageCount - 1)}
            disabled={!canNextPage}
          >
            {">>"}
          </button>{" "}
        </div>
        <div className="goto">
          <span>
            <strong>Go to page: </strong>
            <input
              id="input_page"
              type="number"
              defaultValue={pageIndex + 1}
              onChange={(e) => {
                const page = e.target.value ? Number(e.target.value) - 1 : 0;
                gotoPage(page);
              }}
            />
          </span>{" "}
          <select
            value={pageSize}
            onChange={(e) => {
              setPageSize(Number(e.target.value));
            }}
          >
            {[10, 20, 30, 40, 50].map((pageSize) => (
              <option key={pageSize} value={pageSize}>
                Show {pageSize}
              </option>
            ))}
          </select>
        </div>
      </div>
    </div>
  );
}

/**
 * withAuthenticationRequired is used to make the page protected
 * users will be redirected to the login page if not authorized.
 */
export default Members;
