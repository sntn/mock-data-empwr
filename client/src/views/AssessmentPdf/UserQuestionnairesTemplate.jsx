import React from "react";
import {
  Document,
  Page,
  Text,
  View,
  StyleSheet,
  Image,
} from "@react-pdf/renderer";

// Create styles
const styles = StyleSheet.create({
  page: {
    flexDirection: "row",
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1,
    border: "10px solid red",
  },

  // cover styles
  cover: {
    flexGrow: 1,
    margin: 20,
    padding: 20,
    border: "4px solid #00877A",
    borderRadius: 15,
    backgroundColor: "#F0FFF0",
    color: "#003C36",

    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  coverHeader: {
    fontSize: 26,
    fontWeight: "bold",
    marginBottom: 10,
    color: "#00877A",
  },

  questionnaireContainer: {
    // marginTop: 20,
    marginBottom: 16,
    marginHorizontal: 20,
    padding: 8,
    backgroundColor: "#00877A",
    color: "#ffffff",
    fontSize: 16,
    borderRadius: 5,
    textAlign: "center",
  },
});

const letters = "abcdefghijklmnopqrstuvwxyz";

const UserQuestionnairesTemplate = ({ pages, username }) => {
  // console.log(pages);
  const countQnirs = () => {
    let c = 0;
    for (let i = 0; i < pages.length; i++) {
      let page = pages[i];
      for (let j = 0; j < page.length; j++) {
        if (page[j].type === "HEADER") c++;
      }
    }

    return c;
  };

  const countQns = () => {
    let c = 0;
    for (let i = 0; i < pages.length; i++) {
      let page = pages[i];
      for (let j = 0; j < page.length; j++) {
        if (page[j].type !== "HEADER") c++;
      }
    }

    return c;
  };

  let qnirNo = 0;
  let qnNo = 0;

  return (
    <Document>
      <Page size="A4" style={styles.page}>
        <View style={styles.cover}>
          <Text style={styles.coverHeader}>
            All the questions of assessment 1
          </Text>
          <Text style={styles.coverHeader}>User: {username}</Text>
          <Text>Questionnnaires: {countQnirs()}</Text>
          <Text>Questions: {countQns()}</Text>
        </View>
      </Page>
      {pages.map((page, i) => {
        return (
          <Page key={i} size="A4" style={{ paddingVertical: 20 }}>
            {page.map((el, j) => {
              if (el.type == "HEADER") {
                qnirNo += 1;
                qnNo = 0;
                return (
                  <View key={j} style={styles.questionnaireContainer}>
                    <Text>
                      {qnirNo}&nbsp;.&nbsp;{el.label}
                    </Text>
                  </View>
                );
              } else {
                qnNo += 1;
                return (
                  <View
                    key={j}
                    style={{ marginBottom: 16, marginHorizontal: 20 }}
                  >
                    <Text style={{ fontSize: 13, color: "#00877A" }}>
                      {qnirNo}.{qnNo}&nbsp;.&nbsp;{el.question}
                      &nbsp;
                      {el.type === "MULTISELECT" && "[multiselect]"}
                    </Text>
                    {el.type === "SPECIAL" && (
                      <Text style={{ marginTop: 10, fontSize: 12 }}>
                        Ans: {el.givenAnswer[0]}
                      </Text>
                    )}
                    {el.choices.map((choice, k) => (
                      <View
                        key={k}
                        style={{
                          marginTop: k === 0 ? 10 : 4,
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                        }}
                      >
                        {el.type === "MULTISELECT" ? (
                          el.givenAnswer.includes(choice) ? (
                            <Image
                              style={{ height: 12, width: 12 }}
                              src={process.env.PUBLIC_URL + "/icons/check.png"}
                            />
                          ) : (
                            <Image
                              style={{ height: 12, width: 12 }}
                              src={
                                process.env.PUBLIC_URL + "/icons/uncheck.png"
                              }
                            />
                          )
                        ) : el.type === "MULTICHOICE" ? (
                          <Text
                            style={{
                              color: el.givenAnswer.includes(choice)
                                ? "#38bdf8"
                                : "#334155",
                              fontSize: 12,
                            }}
                          >
                            {letters[k] + ". "}
                          </Text>
                        ) : (
                          ""
                        )}
                        <Text
                          key={k}
                          style={{
                            color: el.givenAnswer.includes(choice)
                              ? "#38bdf8"
                              : "#334155",
                            fontSize: 12,
                          }}
                        >
                          {" "}
                          {choice}
                        </Text>
                      </View>
                    ))}
                  </View>
                );
              }
            })}
          </Page>
        );
      })}
    </Document>
  );
};

export default UserQuestionnairesTemplate;
