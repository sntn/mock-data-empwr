import {
  Document,
  Page,
  Text,
  View,
  StyleSheet,
  Image,
} from "@react-pdf/renderer";

const styles = StyleSheet.create({
  header: {
    paddingRight: 32,
    paddingLeft: 32,
    paddingTop: 36,
    fontSize: 22,
    fontFamily: "Helvetica-Bold",
  },
  headerTxtContainer: {
    borderBottom: "2px solid gray",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row",
  },
  userInfo: {
    paddingRight: 28,
    paddingLeft: 28,
    paddingTop: 12,
    paddingBottom: 10,
    marginTop: 15,
    backgroundColor: "#79bfac",

    display: "flex",
    justifyContent: "center",
    flexDirection: "row",
    flexWrap: "wrap",
  },
  userTextContainer: {
    width: "48%",
    padding: 4,
    marginRight: 4,
    marginLeft: 4,
    marginBottom: 6,
  },
  userInfoText: {
    fontSize: 14,
    fontFamily: "Helvetica-Bold",
  },

  tableContainer: {
    // border: "1px solid red",
    paddingRight: 24,
    paddingLeft: 24,
    marginTop: 15,
  },
  thRow1: {
    backgroundColor: "#109375",
    paddingTop: 3,
    paddingBottom: 3,
    width: "50%",
  },
  thRow1Text: {
    color: "white",
    fontSize: 16,
    textAlign: "center",
    fontFamily: "Helvetica-Bold",
  },
  thRow2: {
    marginTop: 5,
    marginBottom: 5,
  },
  thRow2Txt: {
    color: "#6a6a6a",
    fontSize: 10,
    textAlign: "center",
  },

  choiceCell: {
    width: "12.5%",
    // border: "1px solid red",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  choiceRadio: {
    height: 24,
    width: 24,
    backgroundColor: "#eaeaea",
    borderRadius: "50%",

    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  choiceSelector: {
    height: 12,
    width: 12,
    borderRadius: "50%",
    backgroundColor: "gray",
  },
  footerContainer: {
    marginTop: 24,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    // border: "1px solid black",
  },
  resultContainer: {
    backgroundColor: "#109375",
    width: "20%",
    padding: 5,
    borderRadius: 5,
  },
  resultText: {
    color: "white",
    fontSize: 16,
    textAlign: "center",
    fontFamily: "Helvetica-Bold",
  },
  scaleTxt1: {
    fontSize: 14,
    fontFamily: "Helvetica-Bold",
    textAlign: "center",
    color: "white",
  },
  scaleTxt2: { fontSize: 12, textAlign: "center", color: "white" },
});

const PeriodicAssessmentTemplate = ({ data }) => {
  // console.log("data: ", data);
  const getDate = () => {
    const date = new Date();
    return (
      date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear()
    );
  };

  return (
    <Document>
      <Page size="A4">
        {/* Top header section with logo */}
        <View style={styles.header}>
          <View style={styles.headerTxtContainer}>
            <Text style={{ color: "#0a7373" }}>Periodic Check up Result</Text>
            <Image
              style={{ height: 40, objectFit: "contain", marginBottom: 5 }}
              src={process.env.PUBLIC_URL + "/logos/wellness.png"}
            />
          </View>
        </View>

        {/* User information section */}
        <View style={styles.userInfo}>
          <View style={styles.userTextContainer}>
            <Text style={styles.userInfoText}>Name: Sougata Mondal</Text>
          </View>
          <View style={styles.userTextContainer}>
            <Text style={styles.userInfoText}>
              Email: sougatamondal@gmail.com
            </Text>
          </View>
          <View style={styles.userTextContainer}>
            <Text style={styles.userInfoText}>Phone: 1234123412</Text>
          </View>
          <View style={styles.userTextContainer}>
            <Text style={styles.userInfoText}>Gender: Male</Text>
          </View>
          <View style={styles.userTextContainer}>
            <Text style={styles.userInfoText}>Age: 22</Text>
          </View>
          <View style={styles.userTextContainer}>
            <Text style={styles.userInfoText}>Date: {getDate()}</Text>
          </View>
        </View>

        {/* Questions table */}
        <View style={styles.tableContainer}>
          {/* Questions table header */}
          <View
            style={{ display: "flex", flexDirection: "row", flexWrap: "wrap" }}
          >
            <View
              style={{
                ...styles.thRow1,
                borderRight: "1px solid white",
              }}
            >
              <Text style={styles.thRow1Text}>QUESTIONS</Text>
            </View>
            <View
              style={{
                ...styles.thRow1,
                borderLeft: "1px solid white",
              }}
            >
              <Text style={styles.thRow1Text}>RATING SCALE</Text>
            </View>
            <View style={{ ...styles.thRow2, width: "50%" }}></View>
            <View style={{ ...styles.thRow2, width: "12.5%" }}>
              <Text style={styles.thRow2Txt}>Not at all</Text>
            </View>
            <View style={{ ...styles.thRow2, width: "12.5%" }}>
              <Text style={styles.thRow2Txt}>Several Days</Text>
            </View>
            <View style={{ ...styles.thRow2, width: "12.5%" }}>
              <Text style={styles.thRow2Txt}>More than half days</Text>
            </View>
            <View style={{ ...styles.thRow2, width: "12.5%" }}>
              <Text style={styles.thRow2Txt}>Everyday</Text>
            </View>
          </View>

          {/* table body */}
          {data.map((el) => (
            <View
              key={el.id.$oid}
              style={{
                marginBottom: 8,
                display: "flex",
                flexDirection: "row",
              }}
            >
              <View
                style={{
                  padding: 5,
                  backgroundColor: "#eaeaea",
                  width: "50%",
                }}
              >
                <Text style={{ fontSize: 12, color: "#6a6a6a" }}>
                  {el.question}
                </Text>
              </View>
              <View style={styles.choiceCell}>
                <View style={styles.choiceRadio}>
                  {el.answer === 0 && (
                    <View style={styles.choiceSelector}></View>
                  )}
                </View>
              </View>
              <View style={styles.choiceCell}>
                <View style={styles.choiceRadio}>
                  {el.answer === 1 && (
                    <View style={styles.choiceSelector}></View>
                  )}
                </View>
              </View>
              <View style={styles.choiceCell}>
                <View style={styles.choiceRadio}>
                  {el.answer === 2 && (
                    <View style={styles.choiceSelector}></View>
                  )}
                </View>
              </View>
              <View style={styles.choiceCell}>
                <View style={styles.choiceRadio}>
                  {el.answer === 3 && (
                    <View style={styles.choiceSelector}></View>
                  )}
                </View>
              </View>
            </View>
          ))}

          {/* table footer / result section */}
          <View style={styles.footerContainer}>
            <View style={{ display: "flex", flexDirection: "row" }}>
              <View
                style={{ width: 70, padding: 5, backgroundColor: "lightgreen" }}
              >
                <Text style={styles.scaleTxt1}>0-4</Text>
                <Text style={styles.scaleTxt2}>Minimal</Text>
              </View>
              <View
                style={{ width: 70, padding: 5, backgroundColor: "yellow" }}
              >
                <Text style={styles.scaleTxt1}>5-9</Text>
                <Text style={styles.scaleTxt2}>Mild</Text>
              </View>
              <View
                style={{ width: 70, padding: 5, backgroundColor: "orange" }}
              >
                <Text style={styles.scaleTxt1}>10-14</Text>
                <Text style={styles.scaleTxt2}>Moderate</Text>
              </View>
              <View style={{ width: 70, padding: 5, backgroundColor: "red" }}>
                <Text style={styles.scaleTxt1}>15-21</Text>
                <Text style={styles.scaleTxt2}>Severe</Text>
              </View>
            </View>
            <View style={styles.resultContainer}>
              <Text style={styles.resultText}>Score 45</Text>
            </View>
          </View>
        </View>
      </Page>
    </Document>
  );
};

export default PeriodicAssessmentTemplate;
