import React, { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import { PDFViewer, pdf, BlobProvider } from "@react-pdf/renderer";
import PeriodicAssessmentTemplate from "./PeriodicAssessmentTemplate";

const PeriodicAssessmentPage = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);
  const { user, assessmentVersion } = useParams();
  // console.log({ user, assessmentVersion });

  const baseurl =
    !process.env.NODE_ENV || process.env.NODE_ENV === "development"
      ? "http://localhost:5000"
      : "";

  useEffect(() => {
    axios
      .get(
        `${baseurl}/api/assessment/periodic?user=${user}&assessmentVersion=${assessmentVersion}`
      )
      .then((res) => {
        // console.log("api resp: ", res.data);
        setData(res.data.data);
      })
      .catch((error) => {
        console.error(error.message);
        setError(error.message);
      });
  }, []);

  const uploadNewPdf = async (pdfBlob) => {
    console.log("uploading pdf");

    const formData = new FormData();
    formData.append("file", pdfBlob);

    axios
      .post(`${baseurl}/api/assessment/get-blob`, formData, {
        headers: {
          "Content-Type": "application/pdf",
        },
      })
      .then((resp) => console.log("file upload resp: ", resp.data))
      .catch((err) => console.error(err));
  };

  if (error) return <div id="error">{error}</div>;

  if (data.length === 0) return <div>loading...</div>;

  return (
    <div id="content">
      {/* <PDFViewer
        style={{
          height: "99vh",
          width: "99vw",
        }}
      >
        <PeriodicAssessmentTemplate data={data} />
      </PDFViewer> */}
      <BlobProvider document={<PeriodicAssessmentTemplate data={data} />}>
        {({ blob, url, loading, error }) => {
          if (blob) {
            console.log("sending file to backend", { blob, url });
            uploadNewPdf(blob);
          }
          if (loading) return "Almost done...";
          else {
            return (
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  marginTop: "1rem",
                }}
              >
                <a
                  className="user-pdf-download-btn"
                  href={url}
                  target="_blank"
                  rel="noreferrer"
                >
                  View/Download PDF
                </a>
              </div>
            );
          }
        }}
      </BlobProvider>
    </div>
  );
};

export default PeriodicAssessmentPage;
