import React, { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import { BlobProvider } from "@react-pdf/renderer";
import UserQuestionnairesTemplate from "./UserQuestionnairesTemplate";
import InvalidToken from "./InvalidToken";
import SkeletonDocumentLoading from "../../components/loading/SkeletonDocumentLoading";

const UserAssessmentQuestionsPage = () => {
  const [url, setUrl] = useState(null);
  const [data, setData] = useState(null);
  const [username, setUsername] = useState("");
  const { token } = useParams();
  console.log(token);

  const baseurl =
    !process.env.NODE_ENV || process.env.NODE_ENV === "development"
      ? "http://localhost:5000"
      : "";

  useEffect(() => {
    if (!token) {
      setData("missing params");
      return;
    }

    axios
      .get(`${baseurl}/api/assessment/user?token=${token}`)
      .then((res) => {
        console.log("all-user-questions resp:", res.data);
        if (res.data.success) {
          if (res.data.isPdfAvailable) {
            setUrl(res.data.pdfUrl);
          } else {
            setData(res.data.dataInPages);
            setUsername(res.data.username);
            uploadNewPdf();
          }
        } else {
          setData("error");
          console.error(res);
        }
      })
      .catch(({ response }) => {
        setData(response.data.message);
        console.error(response);
      });
  }, []);

  const uploadNewPdf = async (pdfBlob) => {
    console.log("uploading pdf");

    const formData = new FormData();
    formData.append("file", pdfBlob);

    axios
      .post(`${baseurl}/api/assessment/upload-pdf`, formData, {
        headers: {
          "Content-Type": "application/pdf",
          Authorization: `Bearer ${token}`,
        },
      })
      .then((resp) => console.log("file upload resp: ", resp.data))
      .catch((err) => console.error(err));
  };

  if (data === "error")
    return (
      <div className="container">
        <p>Something went wrong. Please try again later</p>
      </div>
    );
  if (data === "missing params")
    return (
      <div className="container">
        <p>Please provide proper parameters</p>
      </div>
    );
  if (data === "invalid token") return <InvalidToken />;

  return (
    <div className="container">
      {!data && !url && <SkeletonDocumentLoading />}
      {url && (
        <div id="content">
          <iframe
            id="content"
            src={url}
            title="User Assessment Questions"
            style={{
              width: "100%",
              height: "100vh",
              border: "none",
            }}
          />
        </div>
      )}
      {data && (
        <div id="content">
          <BlobProvider
            document={
              <UserQuestionnairesTemplate pages={data} username={username} />
            }
          >
            {({ blob, url, loading, error }) => {
              if (blob) {
                console.log("sending file to backend", { blob });
                uploadNewPdf(blob);
              }
              if (loading) return "Almost done...";
              else {
                return (
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      marginTop: "1rem",
                    }}
                  >
                    <a
                      className="user-pdf-download-btn"
                      href={url}
                      target="_blank"
                      rel="noreferrer"
                    >
                      View/Download PDF
                    </a>
                  </div>
                );
              }
            }}
          </BlobProvider>
        </div>
      )}
    </div>
  );
};

export default UserAssessmentQuestionsPage;
