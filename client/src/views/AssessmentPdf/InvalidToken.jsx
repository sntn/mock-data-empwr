import React from "react";

const InvalidToken = () => {
  return (
    <div
      className="container"
      style={{
        border: "1px solid",
        height: "99vh",
        display: "flex",
        justifyContent: "center",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <img
        src="/404.svg"
        style={{ height: 240, objectFit: "contain" }}
        alt=""
      />
      <h3 style={{ marginTop: "2rem" }}>Could not generate assessment pdf</h3>
    </div>
  );
};

export default InvalidToken;
