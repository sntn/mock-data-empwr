import axios from "axios";
import React, { useEffect, useState } from "react";
import { withAuthenticationRequired } from "@auth0/auth0-react";
import { PDFViewer } from "@react-pdf/renderer";

import AllQuestionnairesTemplate from "./AllQuestionnairesTemplate";
import Loading from "../../components/loading/Loading";
import SkeletonDocumentLoading from "../../components/loading/SkeletonDocumentLoading";

const AllAssessmentQuestionsPage = () => {
  const [data, setData] = useState(null);
  const baseurl =
    !process.env.NODE_ENV || process.env.NODE_ENV === "development"
      ? "http://localhost:5000"
      : "";

  useEffect(() => {
    axios
      .get(`${baseurl}/api/assessment/all`)
      .then((res) => {
        console.log(res.data);
        if (res.data.success) {
          setData(res.data.dataInPages);
        } else {
          setData("error");
          console.error(res);
        }
      })
      .catch((err) => {
        setData("error");
        console.error(err);
      });
  }, []);

  if (data === "error")
    return (
      <div className="container">
        <p>Something went wrong. Please try again later</p>
      </div>
    );

  return (
    <div className="container">
      {!data && <SkeletonDocumentLoading />}
      {data && (
        <PDFViewer
          style={{
            height: "100vh",
            width: "100vw",
          }}
        >
          <AllQuestionnairesTemplate pages={data} />
        </PDFViewer>
      )}
    </div>
  );
};

export default withAuthenticationRequired(AllAssessmentQuestionsPage, {
  onRedirecting: () => <Loading />,
});
