import "./Dashboard.css";
import { Bad, Great, Neutral, Happy, Worst } from "../../icons/mood";
import { Nodata } from "../../icons/dashboard";
import { Good, Danger, Warning } from "../../icons/status";
import React from "react";
import { useTable, useSortBy, usePagination } from "react-table";
import { useRecoilValue } from "recoil";
import { filteredDataState } from "./DashboardState";

function DailyTable() {
  //Mapping of all icons displayed by the table.
  const iconMap = {
    Bad: Bad,
    Great: Great,
    Good: Happy,
    OK: Neutral,
    Terrible: Worst,
    good: Good,
    danger: Danger,
    warning: Warning,
  };

  /**
   * filteredData:List<Object> contains the list of records of the selected range
   * range is 'daily' by default. It is initialized with the array of objects
   * consisting of the records of the corresponding range.
   */
  const filteredData = useRecoilValue(filteredDataState);

  /**
   * Stores the filtered data in a memo so that it can be served to the table.
   */
  const dummyData = React.useMemo(() => filteredData, [filteredData]);

  /**
   * columns - Contains information about the columns of the table
   * which are passed as column options.
   * Header -> shown in the table header.
   * accessor -> used to access indivisual columns.
   * tiptext -> helptext that visible on hovering over the Header.
   * Cell -> a function thatformats the value of a particular column.
   */
  const columns = React.useMemo(
    () => [
      {
        Header: "Name",
        accessor: "name",
        id: "tag",
        tiptext: "Name of the patient",
      },
      {
        Header: "Email",
        accessor: "email",
        tiptext: "Email id of the patient",
      },
      {
        Header: "Sleep Hours",
        accessor: "sleepScore",
        tiptext: "The logged sleep for last day in hours",
      },
      {
        Header: "Meditation Hours",
        accessor: "meditationScore",
        tiptext: "The logged meditation time for last day in hours",
      },
      {
        Header: "Exercise Hours",
        accessor: "exerciseScore",
        tiptext: "The logged exercise time for last day in hours",
      },
      {
        Header: "Mood",
        accessor: "mood",
        tiptext: "The logged mood for last day",
        Cell: (props) =>
          props.value == "_" ? (
            <span>{props.value}</span>
          ) : (
            <img src={iconMap[props.value]} width="20px" height="20px" />
          ),
      },
      {
        Header: "Status",
        accessor: "status",
        tiptext: "The recent status of the patient",
        Cell: (props) => {
          return <img src={iconMap[props.value]} width="20px" height="20px" />;
        },
      },
    ],
    []
  );

  // Creates an instance of the table.
  const tableInstance = useTable(
    { columns, data: dummyData, initialState: { pageIndex: 0 } },
    useSortBy,
    usePagination
  );

  /**
   * headerGroups -> An array of object.
   * It containins a flattened array of final column objects for that row.
   * It has a header field this header field is used to map the colums of the table.
   * headerGroups.header = [{
   *  Header, //the header name
   *  tiptext //the text that will be shown once we hover over the header
   * }]
   *
   * page -> An array of rows for the current page, determined by the current pageIndex value.
   *
   * //pageination variables
   * The dataset is divided into pages.
   *
   * pageSize -> number of records in each page, 10 by default
   * setPageSize -> function to change the no of records in each page.
   * Takes an integer argument.
   * pageCount -> returns total number of pages.
   * pageIndex -> returns the current page's index, 0 by default
   * gotoPage -> function to move between pages. Takes an integer argument - the page number.
   * canPreviousPage -> boolean variable. Returns false if there is no previous page.
   * canNextPage -> boolean variable. Returns false if there is no next page.
   * pageOptions -> array of zero-based index integers corresponding to available pages in the table.
   *
   * For more information, check out the documentation of react-table at https://react-table-v7.tanstack.com/
   */
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = tableInstance;

  /**
   * Component displayed on absence of user records.
   */
  if (filteredData.length == 0)
    return (
      <div className="nodata">
        <img src={Nodata} width="80px" height="80px" />
        <p>No data available</p>
      </div>
    );

  return (
    <div>
      <div className="wrapper">
        <table className="table" {...getTableProps()}>
          <thead>
            {headerGroups.map((headerGroup) => (
              <tr className="row header" {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column, index) => (
                  <th
                    className="cell new-tooltip"
                    {...column.getHeaderProps(column.getSortByToggleProps())}
                  >
                    {column.render("Header")}
                    {headerGroup.headers[index].tiptext && (
                      <span className="tooltip">
                        {headerGroup.headers[index].tiptext}
                      </span>
                    )}
                    <span className="sort-options">
                      {column.isSorted
                        ? column.isSortedDesc
                          ? " 🔽"
                          : " 🔼"
                        : ""}
                    </span>
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {page.map((row) => {
              prepareRow(row);
              return (
                <tr
                  className="row"
                  {...row.getRowProps()}
                  onClick={() =>
                    window.open(
                      `/dashboard/detailed?email=${row.values.email}`,
                      "_blank"
                    )
                  }
                >
                  {row.cells.map((cell) => {
                    return (
                      <td
                        className="cell"
                        {...cell.getCellProps({
                          className:
                            cell.column.id === "tag"
                              ? "tag " + dummyData[row.index].status
                              : null,
                        })}
                      >
                        {cell.render("Cell")}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      <div className="pagination">
        <div className="controls">
          <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
            {"<<"}
          </button>{" "}
          <button onClick={() => previousPage()} disabled={!canPreviousPage}>
            {"<"}
          </button>{" "}
          <span>
            Page{" "}
            <strong>
              {pageIndex + 1} of {pageOptions.length}
            </strong>{" "}
          </span>
          <button onClick={() => nextPage()} disabled={!canNextPage}>
            {">"}
          </button>{" "}
          <button
            onClick={() => gotoPage(pageCount - 1)}
            disabled={!canNextPage}
          >
            {">>"}
          </button>{" "}
        </div>
        <div className="goto">
          <span>
            <strong>Go to page: </strong>
            <input
              id="input_page"
              type="number"
              defaultValue={pageIndex + 1}
              onChange={(e) => {
                const page = e.target.value ? Number(e.target.value) - 1 : 0;
                gotoPage(page);
              }}
            />
          </span>{" "}
          <select
            value={pageSize}
            onChange={(e) => {
              setPageSize(Number(e.target.value));
            }}
          >
            {[10, 20, 30, 40, 50].map((pageSize) => (
              <option key={pageSize} value={pageSize}>
                Show {pageSize}
              </option>
            ))}
          </select>
        </div>
      </div>
    </div>
  );
}

export default DailyTable;
