import { atom, selector } from "recoil";

// Stores the search text for persistence across all tabs of the dashboard.
export const searchState = atom({
  key: "searchState",
  default: "",
});

// Stores the dashboard data which consists of data of all ranges.
export const dataState = atom({
  key: "dataState",
  default: {},
});

// Stores the current range selected in the tabs.
export const rangeState = atom({
  key: "rangeState",
  default: "daily",
});

// Filters the data based on the search text.
export const filteredDataState = selector({
    key: 'FilteredData',
    get: ({get}) => {
      const searchText = get(searchState);
      const data = get(dataState);
      const range = get(rangeState);
  
      const filteredData = data[range].filter((item) => {
        if (searchText === "") {
          return true;
        }
        else {
          return item.name.toLowerCase().includes(searchText);
        }
      });

      return filteredData;
    },
  });
