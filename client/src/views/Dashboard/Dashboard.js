import "./Dashboard.css";
import "./Tabs.css";
import Legend from "../../components/legend/Legend";
import DailyTable from "./DashboardOld";
import { Nodata } from "../../icons/dashboard";
import { Bad, Great, Neutral, Happy, Worst } from "../../icons/mood";
import { Good, Danger, Warning } from "../../icons/status";
import getUserData from "../../utils/localstorage";

import React, { useEffect, useState, useRef } from "react";
import { useTable, useSortBy, usePagination } from "react-table";
import Loading from "../../components/loading/Loading";
import { withAuthenticationRequired, useAuth0 } from "@auth0/auth0-react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { useRecoilState, useSetRecoilState, useRecoilValue } from "recoil";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faList, faXmark } from "@fortawesome/free-solid-svg-icons";
import {
  searchState,
  dataState,
  rangeState,
  filteredDataState,
} from "./DashboardState";
import Fab from "@mui/material/Fab";
import Paper from "@mui/material/Paper";
import InputBase from "@mui/material/InputBase";
import IconButton from "@mui/material/IconButton";

function Table() {
  //Mapping of all icons displayed by the table.
  const iconMap = {
    Bad: Bad,
    Great: Great,
    Good: Happy,
    OK: Neutral,
    Terrible: Worst,
    good: Good,
    danger: Danger,
    warning: Warning,
  };

  /**
   * filteredData:List<Object> contains the list of records of the selected range
   * range is 'daily' by default. It is initialized with the array of objects
   * consisting of the records of the corresponding range.
   */
  const filteredData = useRecoilValue(filteredDataState);

  /**
   * Stores the filtered data in a memo so that it can be served to the table.
   */
  const data = React.useMemo(() => filteredData, [filteredData]);

  /**
   * columns - Contains information about the columns of the table
   * which are passed as column options.
   * Header -> shown in the table header.
   * accessor -> used to access indivisual columns.
   * tiptext -> helptext that visible on hovering over the Header.
   * Cell -> a function thatformats the value of a particular column.
   */
  const columns = React.useMemo(
    () => [
      {
        Header: "Name",
        accessor: "name",
        id: "tag",
        tiptext: "Name of the patient",
      },
      {
        Header: "Email",
        accessor: "email",
        tiptext: "Email id of the patient",
      },
      {
        Header: "Sleep Range",
        accessor: "sleepRange",
        tiptext: "The min and max sleep record within the time range",
      },
      {
        Header: "Average Sleep",
        accessor: "averageSleep",
        tiptext: "The average sleep within the time range",
      },
      {
        Header: "Mood Range",
        accessor: "moodRange",
        tiptext: "The best and worst mood between the time range",
      },
      {
        Header: "Average Mood",
        accessor: "averageMood",
        tiptext: "The average mood within the time range",
      },
      {
        Header: "Status",
        accessor: "status",
        tiptext: "The activity status within the time range",
        Cell: (props) => (
          <img src={iconMap[props.value]} width="20px" height="20px" />
        ),
      },
    ],
    []
  );

  // Creates an instance of the table.
  const tableInstance = useTable(
    { columns, data: data, initialState: { pageIndex: 0 } },
    useSortBy,
    usePagination
  );

  /**
   * headerGroups -> An array of object.
   * It containins a flattened array of final column objects for that row.
   * It has a header field this header field is used to map the colums of the table.
   * headerGroups.header = [{
   *  Header, //the header name
   *  tiptext //the text that will be shown once we hover over the header
   * }]
   *
   * page -> An array of rows for the current page, determined by the current pageIndex value.
   *
   * //pageination variables
   * The dataset is divided into pages.
   *
   * pageSize -> number of records in each page, 10 by default
   * setPageSize -> function to change the no of records in each page.
   * Takes an integer argument.
   * pageCount -> returns total number of pages.
   * pageIndex -> returns the current page's index, 0 by default
   * gotoPage -> function to move between pages. Takes an integer argument - the page number.
   * canPreviousPage -> boolean variable. Returns false if there is no previous page.
   * canNextPage -> boolean variable. Returns false if there is no next page.
   * pageOptions -> array of zero-based index integers corresponding to available pages in the table.
   *
   * For more information, check out the documentation of react-table at https://react-table-v7.tanstack.com/
   */
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = tableInstance;

  const getColor = (id, index) => {
    if (id == "averageSleep") {
      return color[data[index].sleepStatus];
    } else if (id == "averageMood") {
      return color[data[index].moodStatus];
    } else return null;
  };

  //Set of colors used in the table.
  const color = {
    good: "#086972",
    warning: "#EEBC09",
    danger: "#FF2727",
  };

  /**
   * Component displayed on absence of user records.
   */
  if (filteredData.length == 0)
    return (
      <div className="nodata">
        <img src={Nodata} width="80px" height="80px" />
        <p>No data available</p>
      </div>
    );

  return (
    <div>
      <div className="wrapper">
        <table className="table" {...getTableProps()}>
          <thead>
            {headerGroups.map((headerGroup) => (
              <tr className="row header" {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column, index) => (
                  <th
                    className="cell new-tooltip"
                    {...column.getHeaderProps(column.getSortByToggleProps())}
                  >
                    {column.render("Header")}
                    {headerGroup.headers[index].tiptext && (
                      <span className="tooltip">
                        {headerGroup.headers[index].tiptext}
                      </span>
                    )}
                    <span className="sort-options">
                      {column.isSorted
                        ? column.isSortedDesc
                          ? " 🔽"
                          : " 🔼"
                        : ""}
                    </span>
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {page.map((row) => {
              prepareRow(row);
              return (
                <tr
                  className="row"
                  {...row.getRowProps()}
                  onClick={() =>
                    window.open(
                      `/dashboard/detailed?email=${row.values.email}`,
                      "_blank"
                    )
                  }
                >
                  {row.cells.map((cell) => {
                    return (
                      <td
                        className="cell"
                        {...cell.getCellProps({
                          className:
                            cell.column.id === "tag"
                              ? "tag " + data[row.index].tag
                              : null,
                          style: {
                            color: getColor(cell.column.id, row.index),
                            fontWeight:
                              cell.column.id == "averageSleep" ||
                              cell.column.id == "averageMood"
                                ? 600
                                : null,
                          },
                        })}
                      >
                        {cell.render("Cell")}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      {/**bottom pagination section */}
      <div className="pagination">
        <div className="controls">
          <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
            {"<<"}
          </button>{" "}
          <button onClick={() => previousPage()} disabled={!canPreviousPage}>
            {"<"}
          </button>{" "}
          <span>
            Page{" "}
            <strong>
              {pageIndex + 1} of {pageOptions.length}
            </strong>{" "}
          </span>
          <button onClick={() => nextPage()} disabled={!canNextPage}>
            {">"}
          </button>{" "}
          <button
            onClick={() => gotoPage(pageCount - 1)}
            disabled={!canNextPage}
          >
            {">>"}
          </button>{" "}
        </div>
        <div className="goto">
          <span>
            <strong>Go to page: </strong>
            <input
              id="input_page"
              type="number"
              defaultValue={pageIndex + 1}
              onChange={(e) => {
                const page = e.target.value ? Number(e.target.value) - 1 : 0;
                gotoPage(page);
              }}
            />
          </span>{" "}
          <select
            value={pageSize}
            onChange={(e) => {
              setPageSize(Number(e.target.value));
            }}
          >
            {[10, 20, 30, 40, 50].map((pageSize) => (
              <option key={pageSize} value={pageSize}>
                Show {pageSize}
              </option>
            ))}
          </select>
        </div>
      </div>
    </div>
  );
}

function Dashboard() {
  // const { user, getAccessTokenSilently } = useAuth0();

  /**userData:Object - stores data about the current user.
   * searchText:String -> Stores the user's search query.
   * data:Object -> Contains data of all ranges along with the search list.
   * e.g. data = {
   *  daily: [] -> array of objects that stores the daily data
   *  weekly: [] -> array of objects that stores the weekly data
   *  monthly: [] -> array of objects that stores the monthly data
   *  search: [] array of objects that stores name and email of the patients
   * }
   * setRange:String -> Stores the range selected using the Range Tab. Default: "daily".
   * modalOpen:Boolean -> Stores the value that controls the visibility of the modal
   * that contains the legend.
   */
  const [userData, setUserData] = useState(undefined);
  const [searchText, setSearchText] = useRecoilState(searchState);
  const [data, setData] = useRecoilState(dataState);
  const setRange = useSetRecoilState(rangeState);

  const [modalOpen, setModalOpen] = useState(false);

  const handleOpen = () => setModalOpen(true);
  const handleClose = () => setModalOpen(false);

  //A list of all possible ranges.
  const rangeList = ["daily", "weekly", "monthly"];

  const day = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];

  const month = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  var today = new Date();

  /** This function returns the suffix of the date
   * e.g. if the date is 23rd, it'll return "rd"
   * or if it is 12th, it'll return "th".
   */
  function dateSuffix() {
    let date = today.getDate();
    var last = date % 10;
    if (date > 10 && date < 14) return "th";
    return last == 1 ? "st" : last == 2 ? "nd" : last == 3 ? "rd" : "th";
  }

  /**This function performs operations on each row of the data
   * to obtain information that is needed to be displayed in the table.
   * @param {Object} row -> Row object of the data.
   * @param {String} range -> String indicating the range to which the data belongs to.
   */
  function rowOpertation(row, range) {
    if (range == "daily") {
      //Determines the status of the row.
      var activityStatus = getActivityStatus(row.inactivity);
      if (activityStatus == "warning") {
        if (row.moodStatus1 == 0) activityStatus = "danger";
      } else if (activityStatus == "good") {
        if (row.moodStatus1 == 0) activityStatus = "danger";
        else if (row.moodStatus2 == 0) activityStatus = "warning";
      }
      row.status = activityStatus;
      row.sleepScore = parseDecimal(row.sleepScore);
      row.meditationScore = parseDecimal(row.meditationScore);
      row.exerciseScore = parseDecimal(row.exerciseScore);

      //Creating the searchlist.
      temp["search"].push({
        name: row.name,
        email: row.email,
      });
    } else {
      row.sleepRange = `${parseDecimal(row.minSleep)} ~ ${parseDecimal(
        row.maxSleep
      )}`;
      row.moodRange = `${parseDecimal(row.minMood)} ~ ${parseDecimal(
        row.maxMood
      )}`;
      row.sleepStatus = getSleepStatus(row.averageSleep);
      row.moodStatus = getMoodStatus(row.averageMood);
      row.status = getActivityStatus(row.inactivity);
      row.tag = getTag(row.sleepStatus, row.moodStatus, row.status);
    }
  }

  const parseDecimal = (val) => {
    if (val == "_") return val;
    return Math.round(parseFloat(val) * 100) / 100;
  };
  /**
   * This functions retrieves the tag for a row.
   * returns 'danger' if either sleep, mood or status value under the danger limit
   * returns 'warning' if either sleep, mood or status value is under the warning limit
   * otherwise returns 'good'
   */
  const getTag = (sleep, mood, status) => {
    if (sleep == "danger" || mood == "danger" || status == "danger")
      return "danger";
    else if (sleep == "warning" || mood == "warning" || status == "warning")
      return "warning";
    else return "good";
  };

  /**
   * This function to retrieves the activity status.
   * if the user is inactivite for more than 5 days returns 'danger'
   * if the user is inactive for 5 days returns 'warning
   * otherwise returns 'good'
   */
  const getActivityStatus = (value) => {
    if (!value || value > 5) return "danger";
    else if (value > 4) return "warning";
    else return "good";
  };

  /**
   * This function retrieves the sleep status of the user.
   * if the average sleep value is lesser than equal to 4 hours, returns 'danger'
   * if the value is between 4 to 7 hours, returns 'warning'
   * otherwise returns 'good'
   */
  const getSleepStatus = (value) => {
    if (value >= 7) return "good";
    else if (value > 4) return "warning";
    else return "danger";
  };

  /**
   * This function retrieves the mood status of the user.
   * if the value is greater than equal to 4, then returns 'good'
   * if it is between 3 and 4, then returns 'warning'
   * if it is lesser than 3, returns 'danger'
   */
  const getMoodStatus = (value) => {
    if (value >= 4) return "good";
    else if (value >= 3) return "warning";
    else return "danger";
  };

  const baseurl =
    !process.env.NODE_ENV || process.env.NODE_ENV === "development"
      ? "http://localhost:5000"
      : "";

  // temp -> temporary variable used to store data of every range
  var temp = {};
  temp.search = [];

  useEffect(() => {
    //Initializing range as daily
    setRange(rangeList[0]);

    /** This function fetches data for a particular range
     * and creates a new key in the temp variable for that range.
     */
    const fetchData = async (range, token) => {
      const response = await fetch(`${baseurl}/api/summary/${range}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      const result = await response.json();
      // console.log("range: ", range, " result: ", result);
      result.forEach((row) => rowOpertation(row, range));
      temp[range] = result;
    };

    /**This function fetches data for all three ranges
     * and stores it in the data variable.
     */
    const getData = async () => {
      try {
        // const token = await getAccessTokenSilently({
        //   audience: "https://quicksight/",
        // });
        const token = "sampleToken";

        const tempUserData = await getUserData(/* user.sub, token */);

        await fetchData("daily", token);
        await fetchData("weekly", token);
        await fetchData("monthly", token);
        setUserData(tempUserData);
        setData(temp);
      } catch (e) {
        console.error(e);
      }
    };

    getData();
  }, []);

  if (
    (data &&
      Object.keys(data).length === 0 &&
      Object.getPrototypeOf(data) === Object.prototype) ||
    !userData
  )
    return <Loading />;
  return (
    <>
      <div id="dashboard">
        {/**Header section to show the username and the current date with searchbar */}
        <div className="client_logo">
          {userData && userData.log && (
            <img
              src={userData.logo}
              height="50px"
              alt={`${userData.client} Logo`}
            />
          )}
        </div>
        <div className="heading">
          <p>Hi, {userData.name}</p>

          <p>
            {day[today.getDay()] +
              ", " +
              month[today.getMonth()] +
              " " +
              today.getDate() +
              dateSuffix() +
              ", " +
              today.getFullYear()}
          </p>
        </div>
        <SearchBar search={searchText} setSearchText={setSearchText} />

        {/**Main content to show the table of records */}
        <Tabs
          onSelect={(index) => {
            setRange(rangeList[index]);
          }}
        >
          <TabList>
            <Tab>Daily</Tab>
            <Tab>Week</Tab>
            <Tab>Month</Tab>
          </TabList>

          <TabPanel>
            <DailyTable />
          </TabPanel>
          <TabPanel>
            <Table />
          </TabPanel>
          <TabPanel>
            <Table />
          </TabPanel>
        </Tabs>

        {/**Bottom right button to open the Logend */}
        <div className="legend">
          <Fab
            color="primary"
            size="medium"
            aria-label="add"
            onClick={handleOpen}
            sx={{
              backgroundColor: "#086972",
              "&:hover": {
                backgroundColor: "#17b978",
              },
            }}
          >
            <FontAwesomeIcon icon={faList} color="white" />
          </Fab>
        </div>

        <Legend open={modalOpen} close={handleClose} />
      </div>
    </>
  );
}

function SearchBar({ search, setSearchText }) {
  const inputReference = useRef(null);

  /**This fuction converts the search text to lowercase
   * and updates the search variable on change in search text.
   */
  let inputHandler = (e) => {
    var lowerCase = e.target.value.toLowerCase();
    setSearchText(lowerCase);
  };

  /**This function clears the search variable */
  const handleClear = () => {
    setSearchText("");
    inputReference.current.focus();
  };

  return (
    <div className="search" style={{ marginLeft: "4px", marginRight: "4px" }}>
      <Paper
        component="form"
        sx={{
          p: "2px 4px",
          display: "flex",
          alignItems: "center",
        }}
      >
        <InputBase
          sx={{ ml: 1, flex: 1, height: 44 }}
          placeholder="Search"
          inputProps={{ "aria-label": "search" }}
          onChange={inputHandler}
          value={search}
          inputRef={inputReference}
        />
        {search && (
          <IconButton
            sx={{ p: "10px" }}
            aria-label="clear"
            onClick={handleClear}
          >
            <FontAwesomeIcon icon={faXmark} color="#071A52" />
          </IconButton>
        )}
      </Paper>
    </div>
  );
}

export default Dashboard;
