import React from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

const BarChart = ({
  title,
  backgroundColor,
  chartData,
  labels,
  yMax,
  stepSize,
  tooltipLabel = "minutes",
}) => {
  console.log({ yMax, stepSize });
  const options = {
    responsive: true,
    plugins: {
      legend: {
        display: false,
      },
      title: {
        display: true,
        text: title,
        font: {
          size: 18,
          family: "Sans-serif",
        },
      },
      tooltip: {
        displayColors: false,
        callbacks: {
          label: (yDatapoint) => {
            return `${yDatapoint.raw} ${tooltipLabel}`;
          },
        },
      },
    },
    scales: {
      x: {
        grid: {
          display: false,
        },
      },
      y: {
        max: yMax,
        ticks: {
          stepSize,
        },
      },
    },
  };

  const data = {
    labels,
    datasets: [
      {
        data: chartData,
        backgroundColor,
      },
    ],
  };

  return (
    <div style={{ margin: "0 .6rem" }}>
      <Bar options={options} data={data} />
    </div>
  );
};

export default BarChart;
