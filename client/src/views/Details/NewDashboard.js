import React, { useEffect, useRef, useState } from "react";
import axios from "axios";
import { withAuthenticationRequired, useAuth0 } from "@auth0/auth0-react";
import Paper from "@mui/material/Paper";
import InputBase from "@mui/material/InputBase";
import IconButton from "@mui/material/IconButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";

import "./NewDashboard.css";
import DashboardHeader from "./DashboardHeader";
import BarWithLineChart from "./BarWithLineChart";
import ChartLoadingAnimation from "../../components/loading/ChartLoadingAnimation";
import Loading from "../../components/loading/Loading";

let email = "";

function List(props) {
  //Creates a new array by filtering the original array
  const filteredData = props.data.filter((el) => {
    //If no input the return empty array.
    if (props.input === "") {
      return null;
    }
    //Return the item which contains the user input.
    else {
      return (
        el.name.toLowerCase().includes(props.input) ||
        el.email.toLowerCase().includes(props.input)
      );
    }
  });

  if (props.show && filteredData.length > 0)
    return (
      <div className="searchList">
        {filteredData.map((item) => (
          <p
            key={item.email}
            onClick={() => props.handleClick(item.name, item.email)}
          >
            {item.name} <span className="search-email">{item.email}</span>
          </p>
        ))}
      </div>
    );
  else if (props.show && props.input !== "")
    return (
      <div className="no-result">
        <p>No matching search</p>
      </div>
    );
  return <></>;
}

const NewDashboard = () => {
  const [type, setType] = useState("days");
  const [days, setDays] = useState(30);
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [sleepData, setSleepData] = useState(null);
  const [exerciseData, setExerciseData] = useState(null);
  const [meditationData, setMeditationData] = useState(null);
  const [dateLabels, setDateLabels] = useState(null);
  const [isInvalidDate, setIsInvalidDate] = useState(false);
  const [error, setError] = useState(null);
  const [allUsers, setAllUsers] = useState(null);
  const [search, setSearch] = useState("");
  const [showSearch, setShowSearch] = useState(false);

  const inputReference = useRef(null);
  // const { getAccessTokenSilently } = useAuth0();
  const baseurl =
    !process.env.NODE_ENV || process.env.NODE_ENV === "development"
      ? "http://localhost:5000"
      : "";

  const getData = () => {
    const start = new Date(startDate);
    const end = new Date(endDate);
    // console.log("getting dashboard data of: ", email);
    if (end < start) {
      console.log("invalid date range");
      setIsInvalidDate(true);
      return;
    }

    axios
      .get(
        `${baseurl}/api/patient/activities?email=${email}&type=${type}&days=${days}&startDate=${startDate}&endDate=${endDate}`
      )
      .then((res) => {
        if (res.data.success) {
          // console.log("api response: ", res.data);
          setSleepData(res.data.sleepActivities.map((el) => el.minutes));
          setExerciseData(res.data.exerciseActivities.map((el) => el.minutes));
          setMeditationData(
            res.data.meditationActivities.map((el) => el.minutes)
          );
          setDateLabels(res.data.sleepActivities.map((el) => el.date));
          setError(null);
        }
      })
      .catch((err) => {
        console.error(err.response);
        setError(err.response.data.message);
      });
  };

  const formatDateInput = (e, end) => {
    var date = new Date(e.target.value);

    // Extract the day, month, and year
    var day = date.getDate().toString().padStart(2, "0");
    var month = (date.getMonth() + 1).toString().padStart(2, "0");
    var year = date.getFullYear();

    // Format the date as YYYY-MM-DD
    var formattedDate = year + "-" + month + "-" + day;

    // console.log(formattedDate);
    if (end === "start") setStartDate(formattedDate);
    else {
      setEndDate(formattedDate);
    }
  };

  const handleSearch = (e) => {
    if (!showSearch) setShowSearch(true);
    setSearch(e.target.value.toLowerCase());
  };

  const clearSearch = () => {
    setSearch("");
    inputReference.current.focus();
  };

  const handleClick = (name, newemail) => {
    setSearch(name.toLowerCase());
    setShowSearch(false);
    window.history.replaceState(
      null,
      "",
      `/dashboard/detailed?email=${newemail}`
    );
    email = newemail;
    getData();
  };

  useEffect(() => {
    const getToken = async () => {
      // const token = await getAccessTokenSilently({
      //   audience: "https://quicksight/",
      // });
      const token = "sampleToken";
      return token;
    };

    getToken()
      .then((token) => {
        // console.log("got token: ", token);
        axios
          .get(`${baseurl}/api/patient/search`, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((response) => {
            // console.log("search data mdb resp: ", response.data);
            setAllUsers(response.data);
            let url_splitted = window.location.href.split("/");
            let path = url_splitted[url_splitted.length - 1];

            if (path.includes("email")) {
              // console.log(
              //   "setting dashboard email with url email: ",
              //   path.split("=")[1]
              // );
              email = path.split("=")[1];
            } else {
              // console.log(
              //   "setting dashboard email with the first email of the resp: ",
              //   response.data[0].email
              // );
              email = response.data[0].email;
            }

            let userDetails = response.data.find(
              (user) => user.email === email
            );
            if (userDetails) setSearch(userDetails.name.toLowerCase());

            //getting dashboard data
            getData();
          })
          .catch((error) => {
            console.error(error);
          });
      })
      .catch((err) => console.error("get token error: ", err));
  }, []);

  if (!email)
    return (
      <div className="initial__chart__loading__container">
        <ChartLoadingAnimation />
        <p>Fetching details, please wait...</p>
      </div>
    );

  return (
    <div className="outer__dashboard__container">
      <div className="searchbar__container">
        <div>
          <Paper
            component="form"
            sx={{
              p: "2px 4px",
              display: "flex",
              alignItems: "center",
            }}
          >
            <InputBase
              sx={{ ml: 1, flex: 1, height: 44 }}
              placeholder="Search user"
              inputProps={{ "aria-label": "search" }}
              onChange={handleSearch}
              value={search}
              inputRef={inputReference}
            />
            {search && (
              <IconButton
                sx={{ p: "10px" }}
                aria-label="clear"
                onClick={clearSearch}
              >
                <FontAwesomeIcon icon={faXmark} color="#071A52" />
              </IconButton>
            )}
          </Paper>
        </div>
        <div className="searchbar__suggetions">
          {allUsers && (
            <List
              data={allUsers}
              input={search}
              show={showSearch}
              handleClick={handleClick}
            />
          )}
        </div>
      </div>
      {!error ? (
        <div className="dasboard__container">
          <DashboardHeader
            email={email}
            type={type}
            days={days}
            setType={setType}
            setDays={setDays}
            formatDateInput={formatDateInput}
            apply={getData}
          />
          <div className="charts__container">
            <div style={{ height: "50%", width: "50%" }}>
              {sleepData ? (
                <BarWithLineChart
                  title="Sleep"
                  backgroundColor="#0A7373"
                  chartData={sleepData}
                  labels={dateLabels}
                  stepSize={60}
                  tooltipLabel="hours"
                />
              ) : (
                <div className="chart__cell">
                  <ChartLoadingAnimation />
                </div>
              )}
            </div>
            <div style={{ height: "50%", width: "50%" }}>
              {exerciseData ? (
                <BarWithLineChart
                  title="Exercise"
                  backgroundColor="#0A7373"
                  chartData={exerciseData}
                  labels={dateLabels}
                  stepSize={15}
                />
              ) : (
                <div className="chart__cell">
                  <ChartLoadingAnimation />
                </div>
              )}
            </div>
            <div style={{ height: "50%", width: "50%" }}>
              {meditationData ? (
                <BarWithLineChart
                  title="Meditation"
                  backgroundColor="#0A7373"
                  chartData={meditationData}
                  labels={dateLabels}
                  stepSize={15}
                />
              ) : (
                <div className="chart__cell">
                  <ChartLoadingAnimation />
                </div>
              )}
            </div>
          </div>
        </div>
      ) : (
        <div className="not__found__container">
          <img src="/404.png" alt="not found" />
          <p>{error}</p>
        </div>
      )}

      {isInvalidDate && (
        <div className="alert__container">
          <img src="/icons/alert-warning.png" alt="alert warning" />
          <span>Invalid Date Range!</span>
          <img
            onClick={() => setIsInvalidDate(false)}
            src="/icons/alert-close.png"
            alt="alert warning"
          />
        </div>
      )}
    </div>
  );
};

export default NewDashboard;