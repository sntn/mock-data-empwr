import React from "react";
import "./NewDashboard.css";

const DashboardHeader = ({
  type,
  days,
  setDays,
  setType,
  email,
  formatDateInput,
  apply,
}) => {
  return (
    <div className="dashboard__header">
      <p style={{ fontSize: "large", flex: 1 }}>
        Data of <strong>{email}</strong>
      </p>
      <div style={{ display: "flex", alignItems: "center" }}>
        <div className="selectors">
          <div>
            <button
              className={`action_btn ${
                type === "days" && days === 30 ? "activated" : ""
              }`}
              style={{ marginLeft: ".5rem" }}
              onClick={() => {
                setType("days");
                setDays(30);
              }}
            >
              Monthly
            </button>
            <button
              className={`action_btn ${
                type === "days" && days === 7 ? "activated" : ""
              }`}
              onClick={() => {
                setType("days");
                setDays(7);
              }}
            >
              Weekly
            </button>
            <button
              className={`action_btn ${type === "range" ? "activated" : ""}`}
              onClick={() => {
                setType("range");
              }}
            >
              Range
            </button>
          </div>
          {type === "range" && (
            <div className="date_selectors">
              <span style={{ marginLeft: ".8rem" }}>from: </span>
              <input
                className="date__input"
                type="date"
                onChange={(e) => formatDateInput(e, "start")}
              />
              <span style={{ marginLeft: ".8rem" }}>to:</span>
              <input
                className="date__input"
                type="date"
                onChange={(e) => formatDateInput(e, "end")}
              />
            </div>
          )}
        </div>
        <button className="action__apply" onClick={() => apply(email)}>
          See Records
        </button>
      </div>
    </div>
  );
};

export default DashboardHeader;
