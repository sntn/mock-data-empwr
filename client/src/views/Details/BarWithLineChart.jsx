import React from "react";
import {
  Chart as ChartJS,
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip,
  LineController,
  BarController,
} from "chart.js";
import { Chart } from "react-chartjs-2";

ChartJS.register(
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip,
  LineController,
  BarController
);

const BarWithLineChart = ({
  title,
  backgroundColor,
  chartData,
  labels,
  yMax,
  stepSize,
}) => {
  const formatLabel = (val) => {
    if (val < 60) return `${val} min`;
    else
      return `${Math.floor(val / 60)}h ${
        Math.floor(val % 60) !== 0 ? Math.floor(val % 60) + "m" : ""
      }`;
  };

  const options = {
    responsive: true,
    plugins: {
      legend: {
        display: false,
      },
      title: {
        display: true,
        text: title,
        font: {
          size: 18,
          family: "Sans-serif",
        },
      },
      tooltip: {
        displayColors: false,
        callbacks: {
          label: (yDatapoint) => {
            return formatLabel(Math.round(yDatapoint.raw));
          },
        },
      },
    },
    scales: {
      x: {
        grid: {
          display: false,
        },
        title: {
          display: true,
          text: "Dates",
          font: {
            size: 16,
            weight: "bold",
            lineHeight: 1.2,
          },
        },
      },
      y: {
        max: yMax,
        title: {
          display: true,
          text: title + " (in hours)",
          font: {
            size: 12,
            weight: "bold",
          },
        },
        ticks: {
          stepSize,
          callback: function (val) {
            return formatLabel(val);
          },
        },
      },
    },
  };

  const data = {
    labels,
    datasets: [
      {
        type: "line",
        data: chartData,
        backgroundColor: "#fff",
        fill: false,
        borderColor: "#0EA2A2",
        borderWidth: 2,
        lineTension: 0.5,
        radius: 3,
      },
      {
        type: "bar",
        data: chartData,
        backgroundColor,
      },
    ],
  };

  return (
    <div style={{ margin: "0 .6rem" }}>
      <h3>{title}</h3>
      <Chart type="bar" data={data} options={options} />
    </div>
  );
};

export default BarWithLineChart;
