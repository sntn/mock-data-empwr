import React from "react";

const NotFoundPage = () => {
  return (
    <div
      style={{
        height: "90vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <img
        style={{ height: 240, objectFit: "contain" }}
        src="/404.png"
        alt="page not found"
      />
      <p
        style={{
          fontSize: "larger",
          marginTop: "2rem",
          textAlign: "center",
          fontWeight: "500",
          color: "#646464",
        }}
      >
        Could not find the page you
        <br /> were looking for
      </p>
    </div>
  );
};

export default NotFoundPage;
