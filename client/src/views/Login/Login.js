import React, { useEffect } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import Splash from "../../components/splash/Splash";

// Component that handles the login flow.
const Login = () => {
  // const { loginWithRedirect, isAuthenticated } = useAuth0();

  useEffect(() => {
    if (/* isAuthenticated */ true) window.open("/dashboard", "_self");
    // else loginWithRedirect();
  });

  return <Splash/>;
};

export default Login;
