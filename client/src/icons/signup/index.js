import Email from './email.svg';
import Google from './google.svg';
import Facebook from './facebook.svg';
import Copy from './copy.svg';
import Send from './send.svg';
import NoAccount from './no_account.svg';

export {
    Email,
    Google,
    Facebook,
    Copy,
    Send,
    NoAccount
};