import Good from './good.svg';
import Danger from './critical.svg';
import Warning from './warning.svg';

export {
    Good,
    Danger,
    Warning,
};