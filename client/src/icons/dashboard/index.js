import Graph from './graph.svg';
import WGraph from './graph-white.svg';
import Summary from './summary.svg';
import WSummary from './summary_white.svg';
import User from './user.svg';
import WUser from './user-white.svg';
import Manage from './manage.svg';
import WManage from './manage-white.svg';
import Logout from './logout.svg';
import Nodata from './no_data.svg';

export {
    Graph,
    WGraph,
    Summary,
    WSummary,
    User,
    WUser,
    Manage,
    WManage,
    Logout,
    Nodata
};