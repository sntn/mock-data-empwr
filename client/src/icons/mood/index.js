import Bad from './bad.png';
import Great from './great.png';
import Neutral from './neutral.png';
import Happy from './happy.png';
import Worst from './worst.png';

export {
    Bad,
    Great,
    Neutral,
    Happy,
    Worst,
};