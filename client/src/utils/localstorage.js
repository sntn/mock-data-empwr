import date from "date-and-time";

/**This function retrieves the user data from local storage.
 * If data exists and has been created the same day, use the data from localstorage.
 * Otherwise, update the localstorage by making an API call.
 * */
async function getUserData(/* userId, token */) {
  // var userData = JSON.parse(localStorage.getItem("user"));
  // const now = new Date();
  // if (userData) {
  //   const dataCreated = date.parse(userData.createdAt, "DD-MM-YYYY");
  //   if (
  //     date.subtract(now, dataCreated).toDays() < 1 &&
  //     userId == userData.auth0Id
  //   ) {
  //     return userData;
  //   }
  // }
  // // Make api call to get userdata and update the userdata
  // const baseurl =
  //   !process.env.NODE_ENV || process.env.NODE_ENV === "development"
  //     ? "http://localhost:5000"
  //     : "";
  // try {
  //   const requestOptions = {
  //     method: "GET",
  //     headers: {
  //       "Content-Type": "application/json",
  //       Authorization: `Bearer ${token}`,
  //     },
  //   };

  //   const response = await fetch(`${baseurl}/api/user`, requestOptions);

  let result = {
    createdAt: date.format(new Date(), "DD-MM-YYYY"),
    email: "sampleUser@example.com",
    name: "Sample User",
    organisation: "Sample Organization",
    organisationId: "sampleOrganizationId",
  };
  if (/* response.status === 200 */ true) {
    // const result = await response.json();
    localStorage.setItem(
      "user",
      JSON.stringify({
        result,
      })
    );
    return result;
  }
  // else return null;
  // } catch (e) {
  //   console.error(e.message);
  // }
}

export default getUserData;
