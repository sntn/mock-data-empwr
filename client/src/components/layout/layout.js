import { Outlet } from "react-router-dom";
import Sidebar from "../sidebar/Sidebar";
import Loading from "../loading/Loading";
import { withAuthenticationRequired, useAuth0 } from "@auth0/auth0-react";
import UnAuthorized from "../../components/unauthorized/Unauthorized";
import { useState, useEffect } from "react";
import getUserData from "../../utils/localstorage";
import Splash from "../splash/Splash";

/**Declaring the app layout.
 * The width of the sidebar is fixed at 320px.
 */
const AppLayout = () => {
  //const { user, getAccessTokenSilently } = useAuth0();

  const [userData, setUserData] = useState(undefined);

  useEffect(() => {
    const getData = async () => {
      try {
        // const token = await getAccessTokenSilently({
        //   audience: "https://quicksight/",
        // });
        const token = "sampleToken";

        const tempUserData = await getUserData(/* user.sub, token */);

        setUserData(tempUserData);
      } catch (e) {
        console.error(e);
      }
    };

    getData();
  }, []);

  if (userData === undefined) {
    return <Splash />;
  }

  if (userData === null) {
    return <UnAuthorized />;
  }

  return (
    <div
      style={{
        padding: "10px 0px 0px 320px",
      }}
    >
      <Sidebar />
      <Outlet />
    </div>
  );
};

export default AppLayout;
