import React from "react";
import "./ChartLoadingAnimation.css";

const ChartLoadingAnimation = () => {
  return (
    <div className="chart__loading__chart">
      <div className="bar bar1"></div>
      <div className="bar bar2"></div>
      <div className="bar bar3"></div>
      <div className="bar bar4"></div>
    </div>
  );
};

export default ChartLoadingAnimation;
