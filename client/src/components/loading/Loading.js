import React from "react";
import "./loading.scss";

// Component for creating a loading spanner.
const Loading = () => (
  <div className="preloader">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
  </div>
);

export default Loading;
