import React from "react";

const SkeletonDocumentLoading = () => {
  return (
    <div className="skeleton-loading-outer-container">
      <div className="skeleton-container">
        <div className="skeleton-question-container">
          <div className="skeleton-question-text skeleton"></div>
          <div className="skeleton-question-text row-2 skeleton"></div>
          <div className="skeleton-option-text skeleton"></div>
          <div className="skeleton-option-text skeleton"></div>
          <div className="skeleton-option-text skeleton"></div>
          <div className="skeleton-option-text skeleton"></div>
        </div>
        <div>
          <div className="skeleton-question-text skeleton"></div>
          <div className="skeleton-option-text skeleton"></div>
          <div className="skeleton-option-text skeleton"></div>
          <div className="skeleton-option-text skeleton"></div>
        </div>
        <div>
          <div className="skeleton-question-text skeleton"></div>
          <div className="skeleton-question-text row-2 skeleton"></div>
          <div className="skeleton-option-text skeleton"></div>
          <div className="skeleton-option-text skeleton"></div>
          <div className="skeleton-option-text skeleton"></div>
          <div className="skeleton-option-text skeleton"></div>
        </div>
      </div>
      <p className="loading-text">
        Loading document, this might take <br />a couple of minutes...
      </p>
    </div>
  );
};

export default SkeletonDocumentLoading;
