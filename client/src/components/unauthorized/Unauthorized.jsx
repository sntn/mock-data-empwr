import React from "react";
import { useAuth0 } from "@auth0/auth0-react";
import { Logout } from "../../icons/dashboard";

const UnAuthorized = () => {
  const { logout } = useAuth0();

  const logoutWithRedirect = () =>
    logout({
      returnTo: window.location.origin,
    });

  return (
    <div
      className="code"
      style={{
        padding: "5rem",
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          height: "70vh",
          justifyContent: "center",
          alignItems: "center",
          borderRadius: 15,
          boxShadow:
            "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
        }}
      >
        <h1 style={{ color: "#17b978" }}>
          Your are not Authorised to view this dashboard
        </h1>
        <p style={{ fontSize: "large", color: "gray", marginTop: "-.5rem" }}>
          Please login with an appropriate account to view the dashboard
        </p>
        <div
          onClick={() => logoutWithRedirect()}
          style={{
            marginTop: "1rem",
            border: "1px solid",
            width: 200,
            borderRadius: 12,
            display: "flex",
            justifyContent: "center",
          }}
          className="sidebar__footer__item"
        >
          <div className="sidebar__footer__text">Logout</div>
          <div className="sidebar__footer__icon">
            <img src={Logout} height="30px" width="30px" alt="" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default UnAuthorized;
