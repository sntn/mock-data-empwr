import React from "react";
import "./splash.css";

// Splash Screen
const Splash = () => {
  return (
    <div className="splash__container">
      <img
        className="splash__logo"
        src="/logos/wellness.png"
        width="360px"
        alt="wellness connection logo"
      />
    </div>
  );
};

export default Splash;
