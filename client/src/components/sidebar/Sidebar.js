import { useEffect, useRef, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import { useAuth0 } from "@auth0/auth0-react";
import "./sidebar.scss";
import {
  Graph,
  WGraph,
  Summary,
  WSummary,
  Logout,
  User,
  WUser,
  Manage,
  WManage,
} from "../../icons/dashboard";

/**Contains information about all the options in the sidebar.
 * display -> the title of the option
 * icons -> the image icon of the option
 *  active image is shown when the option is selected, otherwise
 *  the inactive image is displayed
 * to -> the path to which the application is redirected to when clicked
 * section -> used to identify current page
 */
const sidebarNavItems = [
  {
    display: "Dashboard",
    icon: {
      active: <img src={WSummary} height="36px" width="36px" alt="" />,
      inactive: <img src={Summary} height="36px" width="36px" alt="" />,
    },
    to: "/dashboard",
    section: "",
  },
  {
    display: "Detailed",
    icon: {
      active: <img src={WGraph} height="36px" width="36px" alt="" />,
      inactive: <img src={Graph} height="36px" width="36px" alt="" />,
    },
    to: "/dashboard/detailed",
    section: "detailed",
  },
  {
    display: "Signup",
    icon: {
      active: <img src={WUser} height="36px" width="36px" alt="" />,
      inactive: <img src={User} height="36px" width="36px" alt="" />,
    },
    to: "/dashboard/signup",
    section: "signup",
  },
  {
    display: "Members",
    icon: {
      active: <img src={WManage} height="36px" width="36px" alt="" />,
      inactive: <img src={Manage} height="36px" width="36px" alt="" />,
    },
    to: "/dashboard/members",
    section: "members",
  },
  // {
  //   display: "Assessment Pdf",
  //   icon: {
  //     active: <img src="/icons/assessment-active.png" height="36px" width="36px" />,
  //     inactive: (
  //       <img src="/icons/assessment-inactive.png" height="36px" width="36px" />
  //     ),
  //   },
  //   to: "/assessment/all",
  //   section: "assessment-pdf",
  // },
];

const Sidebar = () => {
  const { logout } = useAuth0();

  const logoutWithRedirect = () =>
    logout({
      returnTo: window.location.origin,
    });

  /**
   * activeIndex -> contains the index of the selected option in the sidebar
   * stepHeight -> used to show transition when some option is selected
   * location -> used to change the activeIndex
   */
  const [activeIndex, setActiveIndex] = useState(0);
  const [stepHeight, setStepHeight] = useState(0);
  const sidebarRef = useRef();
  const indicatorRef = useRef();
  const location = useLocation();

  useEffect(() => {
    setTimeout(() => {
      const sidebarItem = sidebarRef.current.querySelector(
        ".sidebar__menu__item"
      );
      indicatorRef.current.style.height = `${sidebarItem.clientHeight}px`;
      setStepHeight(sidebarItem.clientHeight);
    }, 50);
  }, []);

  /**Changing active index based on the url.
   * The url is split url using "/" and stored in a list
   * If there is no third item, that means we are in dashboard
   * If there is a third item, then we get the index using sidebarItem section property
   */
  useEffect(() => {
    const curPath = window.location.pathname.split("/")[2] || "";
    const activeItem = sidebarNavItems.findIndex(
      (item) => item.section === curPath
    );
    setActiveIndex(curPath.length === 0 ? 0 : activeItem);
  }, [location]);

  return (
    <div className="sidebar">
      {/* Sidebar logo */}
      <div className="sidebar__logo">
        <img src="/logos/butterfly.png" height="60px" alt="Empwr Logo" />
        <h1>Empwr</h1>
      </div>
      <div ref={sidebarRef} className="sidebar__menu">
        {/* Active selection indicator */}
        <div
          ref={indicatorRef}
          className="sidebar__menu__indicator"
          style={{
            transform: `translateX(-50%) translateY(${
              activeIndex * stepHeight
            }px)`,
          }}
        ></div>
        {/* Sidebar items */}
        {sidebarNavItems.map((item, index) => (
          <Link to={item.to} key={index} style={{ textDecoration: "none" }}>
            <div
              className={`sidebar__menu__item ${
                activeIndex === index ? "active" : ""
              }`}
            >
              <div className="sidebar__menu__item__icon">
                {item.icon[activeIndex === index ? "active" : "inactive"]}
              </div>
              <div className="sidebar__menu__item__text">{item.display}</div>
            </div>
          </Link>
        ))}
      </div>
      {/* The Sidebar Footer */}
    </div>
  );
};

export default Sidebar;
