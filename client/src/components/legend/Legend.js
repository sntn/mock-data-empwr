import React from "react";
import Modal from "react-modal";
import { useRecoilValue } from "recoil";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import { rangeState } from "../../views/Dashboard/DashboardState";

import "./Legend.css";

/**This is a Modal component that explains the
 * color coding used in the table for the selected data range */
const Legend = (props) => {
  Modal.setAppElement("#root");

  const customModalStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      maxHeight: "450px",
      overflowY: "scrool",
    },
  };

  /**Function to close the modal */
  const handleClose = () => props.close();

  return (
    <Modal
      isOpen={props.open}
      onRequestClose={handleClose}
      style={customModalStyles}
      contentLabel="Legend"
    >
      {/**Header section and the close button */}
      <FontAwesomeIcon
        className="close-icon"
        icon={faXmark}
        onClick={handleClose}
        color="#071A52"
      />
      <h2 className="title" style={{ color: "#086972" }}>
        Legend
      </h2>
      {/**Modal content */}
      <Content />
    </Modal>
  );
};

/**
 * Data of the modal is different for different range.
 * This component returns the content of the modal according to
 * the selected range.
 */
const Content = () => {
  /**Retrieving the selected range. */
  const range = useRecoilValue(rangeState);

  //Set of colors used in the table.
  const colorMap = {
    red: "#FF2727",
    green: "#086972",
    yellow: "#EEBC09",
  };

  /**Set of data for different range.
   * Range is used as key to get the content of the modal.
   */
  const legendData = {
    daily: {
      Status: {
        red: "Inactive or Bad mood for 4 or more days",
        yellow: "Inactive or Bad mood for 3 or more days",
        green: "Otherwise",
      },
    },
    weekly: {
      "Average Sleep": {
        green: "Average sleep is greater or equal to 7 hours",
        yellow: "Average sleep is greater than 4 hours but less than 7",
        red: "Otherwise",
      },
      "Average Mood": {
        green: "Average mood rating is greater or equal to 4",
        yellow: "Average mood greater than or equal to 3 but less than 4",
        red: "Otherwise",
      },
      Status: {
        red: "Inactive for 4 or more days",
        yellow: "Inactive for 3 or more days",
        green: "Otherwise",
      },
      ND: "No data - If less than 4 logs have been made in the last 7 days",
    },
    monthly: {
      "Average Sleep": {
        green: "Average sleep is greater or equal to 7 hours",
        yellow: "Average sleep is greater than 4 hours but less than 7",
        red: "Otherwise",
      },
      "Average Mood": {
        green: "Average mood rating is greater or equal to 4",
        yellow: "Average mood greater than or equal to 3 but less than 4",
        red: "Otherwise",
      },
      Status: {
        red: "Inactive for 4 or more days",
        yellow: "Inactive for 3 or more days",
        green: "Otherwise",
      },
      ND: "No data - If less than 20 logs have been made in the last 30 days",
    },
  };

  return (
    <div className="content">
      {Object.keys(legendData[range]).map((category, categoryVal) => (
        <>
          {/**Name of the keys are used to create sucsection */}
          {category != "ND" ? (
            <h3 key={categoryVal} className="subtitle">
              {category}
            </h3>
          ) : (
            <div className="mapping">
              <p>
                <h3 className="color-block" style={{ color: "#FF2727" }}>
                  ND
                </h3>
                {legendData[range][category]}
              </p>
            </div>
          )}
          {/**Values of each subsection is mapped */}
          {category != "ND" &&
            Object.keys(legendData[range][category]).map((code, idx) => (
              <div key={idx} className="mapping">
                <div
                  className="color-block"
                  style={{ backgroundColor: colorMap[code] }}
                ></div>

                <p className="mapping-value">
                  {legendData[range][category][code]}
                </p>
              </div>
            ))}
        </>
      ))}
    </div>
  );
};

export default Legend;
