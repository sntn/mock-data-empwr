import { useRef } from "react";
import Paper from "@mui/material/Paper";
import InputBase from "@mui/material/InputBase";
import IconButton from "@mui/material/IconButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";

function SearchBar({ search, setSearchText }) {
    const inputReference = useRef(null);
  
    /**This fuction converts the search text to lowercase 
     * and updates the search variable on change in search text.
     */
    let inputHandler = (e) => {
      var lowerCase = e.target.value.toLowerCase();
      setSearchText(lowerCase);
    };
  
    /**This function clears the search variable */
    const handleClear = () => {
      setSearchText("");
      inputReference.current.focus();
    };
  
    return (
      <div className="search" style={{marginLeft: "4px", marginRight: "4px"}}>
        <Paper
          component="form"
          sx={{
            p: "2px 4px",
            display: "flex",
            alignItems: "center",
          }}
        >
          <InputBase
            sx={{ ml: 1, flex: 1, height: 44 }}
            placeholder="Search"
            inputProps={{ "aria-label": "search" }}
            onChange={inputHandler}
            value={search}
            inputRef={inputReference}
          />
          {search && (
            <IconButton
              sx={{ p: "10px" }}
              aria-label="clear"
              onClick={handleClear}
            >
              <FontAwesomeIcon icon={faXmark} color="#071A52" />
            </IconButton>
          )}
        </Paper>
      </div>
    );
  }

  export default SearchBar;