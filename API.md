All API access is over HTTPS, and accessed from `https://rpm.empwr.life`. All data is sent and received as JSON.

## Get the summary for a particular time range

Get the summary data of all individuals belonging to the organisation that the user has access to.

### GET `/api/summary/{range}`

### Parameters

#### Path parameters
___

`range` string **required**

The time range for which the data is requested.

Can be among three possible values:

1. daily
2. weekly
3. monthly


### HTTP Response Status Codes

| Status Code | Description |
| - | - |
| 200 | OK |

### Response

#### Example Response

**Status: 200**

```json

```

#### Example Schema

**Status: 200**

```json

```

### GET `/api/user`


### HTTP Response Status Codes

| Status Code | Description |
| - | - |
| 200 | OK |

### Response

#### Example Response

**Status: 200**

```json

```

#### Example Schema

**Status: 200**

```json

```

### GET `/api/user/search`


### HTTP Response Status Codes

| Status Code | Description |
| - | - |
| 200 | OK |

### Response

#### Example Response

**Status: 200**

```json

```

#### Example Schema

**Status: 200**

```json

```


### POST `/api/user/signup`

### Parameters

#### Body parameters

___
`firstName` String **required**

The first name of the patient to be signed up.
___
`lastName` String **required**

The last name of the patient to be signed up.
___
`email` String **required**

The email id of the patient to be signed up.


### HTTP Response Status Codes

| Status Code | Description |
| - | - |
| 200 | OK |

### Response

#### Example Response

**Status: 200**

```json

```

#### Example Schema

**Status: 200**

```json

```

### GET `/api/dashboard/url`


### HTTP Response Status Codes

| Status Code | Description |
| - | - |
| 200 | OK |

### Response

#### Example Response

**Status: 200**

```json

```

#### Example Schema

**Status: 200**

```json

```

## Error Response

```json
{
    "timestamp":"2019-09-16T22:14:45.624+0000",
    "status":500,
    "error":"Internal Server Error",
	"logs": "",
    "message":"Error occurred during retrieval of data from the database",
    "path":"/api/summary/daily"
}
```