const express = require("express");
const User = require("../models/user");
const Invite = require("../models/invite");
const mongoose = require("mongoose");
const { sendEmailWithTemplate } = require("../services/mail");
const { getUserData, checkJwt } = require("../utils/utils");
const axios = require("axios");

const users = require("../data/userMock.json");

var router = express.Router();

router.get("/status", /* checkJwt, */ function (req, res) {
  // getUserData(req.auth.payload.sub)
  //   .then(async function (data) {
  //     try {
  // const invites = await Invite.find(
  //   {
  //     user: {
  //       $ne: null,
  //     },
  //     organisation: data.organisationId,
  //   },
  //   "code createdAt active user"
  // )
  //   .sort({ createdAt: -1 })
  //   .populate(
  //     "user",
  //     "_id email given_name family_name auth0Id lastAssessmentDate organisation"
  //   );

  // var processInvites = {};
  // invites.forEach((invite) => {
  //   if (invite.user) {
  //     if (!processInvites[invite.user["_id"]])
  //       processInvites[invite.user["_id"]] = invite;
  //   }
  // });

  // const inviteList = Object.values(processInvites).sort((a, b) => {
  //   var p1 = a.user.given_name + " " + a.user.family_name;
  //   var p2 = b.user.given_name + " " + b.user.family_name;
  //   return p1.localeCompare(p2);
  // });

  res.status(200).json({
    status: 200,
    count: users.length,
    data: users,
  });
  // } catch (error) {
  //   res.status(500).json({
  //     status: 500,
  //     message: "Error occured while fetching invites",
  //     error: error.stack,
  //   });
  // }
  // })
  // .catch(function (error) {
  //   res.status(500).json({
  //     status: 500,
  //     message: "Error occured while fetching user data",
  //     error: error.stack,
  //   });
  // });
});

router.post("/invite", checkJwt, function (req, res) {
  getUserData(req.auth.payload.sub)
    .then(async function (data) {
      const { email, name, code, user } = req.body;
      if (code) {
        try {
          await sendEmailWithTemplate(
            email,
            "Welcome to your gateway to good health, EMPWR",
            "inviteEmailTemplate",
            {
              USER_NAME: name,
              INVITE_CODE: code,
            }
          );
          res.status(200).json({
            status: 200,
            message: "Email sent successfully",
          });
        } catch (error) {
          res.status(500).json({
            status: 500,
            message: "Error while sending the email",
            error: error.stack,
          });
        }
      } else {
        if (!user) {
          return res.status(400).json({
            status: 400,
            message: "Id of the patient is missing",
            error: error.stack,
          });
        }
        axios({
          method: "post",
          url: "https://api.yubihealth.com/invite",
          data: {
            id: user,
            organisation: data.organisationId,
          },
          headers: {
            Authorization: "Basic " + process.env.ADMIN_APIKEY,
            "Content-Type": "application/json",
          },
        })
          .then((response) => {
            res
              .status(response.status)
              .json({ reload: true, ...response.data });
          })
          .catch((error) => {
            res.status(500).json({
              status: 500,
              message: "Error when creating new invite code",
              error: error.stack,
            });
          });
      }
    })
    .catch(function (error) {
      res.status(500).json({
        status: 500,
        message: "Error occured while fetching user data",
        error: error.stack,
      });
    });
});

module.exports = router;
