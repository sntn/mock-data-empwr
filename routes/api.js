var express = require("express");
// var AWS = require("aws-sdk");
const axios = require("axios");

require("dotenv").config();

const { getUserData, checkJwt } = require("../utils/utils");
const {
  dailySummary,
  weeklySummary,
  monthlySummary,
} = require("../queries/summary");
const { userSearchData, getUserActivities } = require("../queries/user");

var router = express.Router();

router.get(
  "/user",
  /* checkJwt, */ function (req, res) {
    getUserData()
      .then(function (data) {
        if (data) {
          res.status(200).json(data);
        } else {
          res.status(401).json({
            message: "User does not exist in organisation",
            status: 401,
          });
        }
      })
      .catch(function (error) {
        res.status(500).send(error);
      });
  }
);

router.get(
  "/summary/daily",
  /* checkJwt, */ function (req, res) {
    dailySummary()
      .then((resp) => {
        res.status(200).json(resp);
      })
      .catch((err) => res.status(500).json(err));
  }
);

router.get(
  "/summary/weekly",
  /* checkJwt, */ function (req, res) {
    weeklySummary()
      .then((resp) => res.status(200).json(resp))
      .catch((err) => res.status(500).json(err));
  }
);

router.get(
  "/summary/monthly",
  /* checkJwt, */ function (req, res) {
    monthlySummary()
      .then((resp) => res.status(200).json(resp))
      .catch((err) => res.status(500).json(err));
  }
);

router.get(
  "/patient/search",
  /* checkJwt, */ function (req, res) {
    userSearchData()
      .then((resp) => res.status(200).json(resp))
      .catch((err) => res.status(500).send(err));
  }
);

router.get("/patient/activities", function (req, res) {
  console.log(req.query);
  const { email, type, days, startDate, endDate } = req.query;

  if (type === "days" && !days)
    res.status(401).send({
      success: false,
      message: "invalid arguments",
    });
  else if (type === "range" && (!startDate || !endDate))
    res.status(401).send({
      success: false,
      message: "invalid arguments",
    });

  getUserActivities(email, type, days, startDate, endDate)
    .then((resp) => {
      if (resp === "no user found") {
        res.status(404).send({
          success: false,
          message: resp,
        });
      }
      res.status(200).send({ success: true, ...resp });
    })
    .catch((err) => console.error(err));
});

router.post(
  "/patient/signup",
  /* checkJwt, */ function (req, res) {
    const { firstName, lastName, email } = req.body;

    res.status(200).send(`User successfully signed up.`);
  }
);

module.exports = router;
