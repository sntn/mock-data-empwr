const express = require("express");
const ObjectId = require("mongoose").Types.ObjectId;
const multer = require("multer");
const aws = require("aws-sdk");
const fs = require("fs");
require("dotenv").config();

const GlobalConfig = require("../models/globalConfig");
const Questionnaire = require("../models/questionnaire");
const QuestionModel = require("../models/question");
const Answer = require("../models/answer");
const RuleEngineModel = require("../models/ruleEngine");
const User = require("../models/user");
const AssessmentPdfToken = require("../models/assessmentPdfToken");
const data = require("../MOCK_DATA.json");

const {
  formatInPages,
  ruleEngine2,
  traverse,
} = require("../services/assessment-pdf");

// Configure AWS SDK
aws.config.update({
  accessKeyId: process.env.S3_AccessKeyId,
  secretAccessKey: process.env.S3_SecretAccessKey,
  region: process.env.S3_Region,
});

const s3 = new aws.S3();

const upload = multer();

var router = express.Router();

let currentQnir = "";
let ruleEngineQueue = [];

const getNextQuestionnaire2 = async (
  questionnaire,
  userId,
  allUserGivenAnswers,
  assessmentVersion
) => {
  //check rule Engine
  let nextQuestionnaireList = await ruleEngine2(
    questionnaire,
    userId,
    allUserGivenAnswers,
    assessmentVersion
  );
  // console.log("next qnir list: ", nextQuestionnaireList);

  if (nextQuestionnaireList.length === 0) {
    const next = ruleEngineQueue[0];

    //if there are elements in ruleEngineQ, then update CurrentQnir
    currentQnir = next;

    //pop from ruleEngineQ
    //remove first element from ruleEngineQueue
    ruleEngineQueue = ruleEngineQueue.slice(1);

    // console.log("rule eng q: ", ruleEngineQueue);
    return next;
  }

  const newQuestionnaire = nextQuestionnaireList[0];

  //if there are multiple elements in nextQnirLst, then we need to push
  //them in ruleEngQ except the very first one
  if (newQuestionnaire !== currentQnir) {
    //adding all elements of nextqnirlst in reuleEngQ except the first one
    ruleEngineQueue = nextQuestionnaireList.slice(1).concat(ruleEngineQueue);

    currentQnir = nextQuestionnaireList[0];

    // console.log("rule eng q: ", ruleEngineQueue);
    return newQuestionnaire;
  }

  // console.log("rule eng q: ", ruleEngineQueue);
  return newQuestionnaire;
};

const getUnansweredQuestionnaire2 = async (
  beginPoint,
  userId,
  allUserGivenAnswers,
  assessmentVersion
) => {
  let questionnaire = beginPoint;

  questionnaire = await getNextQuestionnaire2(
    questionnaire,
    userId,
    allUserGivenAnswers,
    assessmentVersion
  );

  return questionnaire;
};

const createResp = async (
  givenAnswers,
  userId,
  allUserGivenAnswers,
  allQuestionnaires,
  assessmentVersion
) => {
  // console.log("currentQnir: ", currentQnir);

  let nextQuestionnaireId = currentQnir;
  nextQuestionnaireId = await getUnansweredQuestionnaire2(
    nextQuestionnaireId,
    userId,
    allUserGivenAnswers,
    assessmentVersion
  );

  if (!nextQuestionnaireId) {
    return givenAnswers;
  }

  let { questions, label: qnirLabel } = allQuestionnaires.find((qnir) =>
    qnir._id.equals(ObjectId(nextQuestionnaireId))
  );

  // console.log({
  //   questionNos: questions.length,
  //   qnirLabel,
  // });

  let formattedQuestions = [];

  //update the given response
  questions.map((qn) => {
    const { choice, _id, type, specialType, label } = qn;
    let givenAnswer = allUserGivenAnswers.find((ans) =>
      ans.qId.equals(ObjectId(_id))
    );
    // console.log("got givenAnswer of question: ", label);

    formattedQuestions.push({
      question: label,
      choices: choice.map(function (item) {
        return item["label"];
      }),
      type: type,
      givenAnswer:
        type === "SPECIAL" && givenAnswer.description
          ? [givenAnswer.description]
          : givenAnswer.options,
    });
  });

  givenAnswers.push({
    questionnaire: qnirLabel,
    questions: formattedQuestions,
  });

  // givenAnswers = createResp(givenAnswers, userId);
  givenAnswers = createResp(
    givenAnswers,
    userId,
    allUserGivenAnswers,
    allQuestionnaires,
    assessmentVersion
  );

  return givenAnswers;
};

router.get("/all", async function (req, res) {
  const { tree } = await RuleEngineModel.findOne({
    active: true,
  });

  if (!tree) {
    res.status(404).send({
      success: false,
      message: "rule engine tree not found",
    });
  }

  let allQnirIds = [];
  // if the tree node is actual node instead of connector, then push the qnir id
  // in allQnirIds to get all unique qnirIds.
  tree.map(
    (el) => el._doc.questionnaire && allQnirIds.push(el._doc.questionnaire)
  );

  //getting all questionnnaire with their questions
  const allQuestionnaires = await Questionnaire.find({
    // _id: ObjectId("5fd7f1e0f2a3af00131bfa9a"),
    _id: { $in: allQnirIds },
  })
    .populate({
      path: "questions",
    })
    .catch((e) => {
      console.log(e);
      return null;
    });
  // console.log(allQuestionnaires);

  //inorder traversal of the tree
  const inorder = traverse(tree);

  //formatting all qnirs in inorder form
  let allQnirInorder = [];
  inorder.map((label) =>
    allQnirInorder.push(allQuestionnaires.find((qnir) => qnir.label === label))
  );
  // console.log("allqnir in order: ", allQnirInorder);
  // allQnirInorder.map((qnir) => console.log(qnir._doc.label));
  console.log("allqnir in order length: ", allQnirInorder.length);

  let formattedQnirs = [];
  for (let i = 0; i < allQnirInorder.length; i++) {
    let formattedQns = [];
    for (let j = 0; j < allQnirInorder[i]._doc.questions.length; j++) {
      let qn = {};
      qn["question"] = allQnirInorder[i]._doc.questions[j]._doc.label;
      qn["type"] = allQnirInorder[i]._doc.questions[j]._doc.type;
      qn["choices"] = allQnirInorder[i]._doc.questions[j]._doc.choice.map(
        function (item) {
          return item["label"];
        }
      );
      formattedQns.push(qn);
    }
    let qnir = {
      questionnaire: allQnirInorder[i]._doc.label,
    };
    qnir["questions"] = formattedQns;

    formattedQnirs.push(qnir);
  }
  // console.log("formatted qnirs: ", formattedQnirs);

  // splitting formatted data in pages
  const dataInPages = formatInPages(formattedQnirs);

  res.status(200).send({
    success: true,
    dataInPages,
  });
});

router.get("/user", async function (req, res) {
  const { token } = req.query;

  if (token.length !== 10) {
    return res.status(404).send({
      success: false,
      message: "invalid token",
    });
  }

  const assessmentPdfToken = await AssessmentPdfToken.findOne({
    token,
  });

  if (!assessmentPdfToken) {
    return res.status(404).send({
      success: false,
      message: "invalid token",
    });
  }

  // if there is already a pdfurl in the document, then just return the pdf url
  // console.log("found document: ", assessmentPdfToken);

  const { email, assessmentVersion, pdfUrl, isPdfAvailable } =
    assessmentPdfToken;

  if (assessmentPdfToken.pdfUrl) {
    return res
      .status(200)
      .send({ success: true, isPdfAvailable: true, pdfUrl });
  }

  let givenAnswers = [];

  let {
    _id: userId,
    given_name,
    family_name,
  } = await User.findOne({ email: email });
  let username = given_name + " " + family_name;

  console.log("generating assessment pdf of: ", username);

  if (!userId) {
    return res.status(404).send({
      success: false,
      message: "user not found",
    });
  }

  //read active rule engine and save locally as json
  const ruleEngineConfig = await RuleEngineModel.findOne({ active: true });
  const jsonData = JSON.stringify(ruleEngineConfig, null, 2);
  fs.writeFile("active-rule-engine.json", jsonData, (error) => {
    if (error) console.error(error);
    console.log("rule engine saved locally");
  });

  //get the mandatory qeustionnaires from global config
  const config = await GlobalConfig.findOne({}).catch((e) => null);
  const mandatoryQnirIds = config.qnList;
  //add L0 in mandatory qnir id as it also mandatory
  mandatoryQnirIds.push("5fd7f1e1f2a3af00131bfc58");
  // console.log({ mandatoryQnirIds });

  const allQuestionnaires = await Questionnaire.find({})
    .populate({
      path: "questions",
    })
    .catch((e) => {
      console.log(e);
      res.status(500).send(e);
      return;
    });

  const mandatoryQnirs = allQuestionnaires.filter((qnir) =>
    mandatoryQnirIds.includes(qnir._id)
  );

  const allUserGivenAnswers = await Answer.find({
    user: ObjectId(userId),
    assessmentVersion: parseInt(assessmentVersion),
    // assessmentVersion: 2,
  }).sort({ createdAt: 1 });

  //generating given answers for all mandatory qnirs
  mandatoryQnirs.map((qnir, i) => {
    let questions = [];
    qnir.questions.map((qn) => {
      const { choice, _id, type, specialType, label } = qn;
      // console.log("qnir: ", i + 1, " qn: ", _id, label);

      let givenAnswer = allUserGivenAnswers.find((ans) =>
        ans.qId.equals(ObjectId(_id))
      );
      // console.log({ givenAnswer });
      if (!givenAnswer) {
        res.status(200).send({
          success: false,
          message: "No answers found",
        });
        return;
      }

      // getting all question, answers for this qnir
      questions.push({
        question: label,
        choices: choice.map(function (item) {
          return item["label"];
        }), //getting only the label field as array as we are interested in that only
        type: type,
        givenAnswer:
          type === "SPECIAL" && givenAnswer.description
            ? [givenAnswer.description]
            : givenAnswer.options,
      });
    });

    givenAnswers.push({
      questionnaire: qnir.label,
      questions,
    });
  });

  // to generate next qnirs, setting current qnir as the last mandatory qnir
  currentQnir = mandatoryQnirIds[mandatoryQnirIds.length - 1];

  createResp(
    givenAnswers,
    userId,
    allUserGivenAnswers,
    allQuestionnaires,
    assessmentVersion
  )
    .then(async (resp) => {
      // console.log("got res: ", resp);
      let dataInPages = formatInPages(resp);
      res.status(200).send({
        success: true,
        username,
        dataInPages,
        isPdfAvailable: false,
      });
    })
    .catch((err) => {
      console.error(err);
      res.status(500).send(err);
    });
});

router.post("/upload-pdf", upload.single("file"), async function (req, res) {
  const file = req.file;

  const authorization = req.headers.authorization;
  const token = authorization.split(" ")[1];

  const assessmentPdfToken = await AssessmentPdfToken.findOne({
    token,
  });
  if (!assessmentPdfToken) {
    res.status(404).send({
      success: false,
      error: "Invalid Token",
    });
    return;
  }

  console.log("Received file:", file);
  // console.log("file buffer: ", file.buffer);
  console.log("token: ", token);

  const blobData = file.buffer;

  const params = {
    Bucket: process.env.S3_BucketName,
    Key: `${token}.pdf`,
    Body: blobData,
    ContentType: "application/pdf",
    ACL: "public-read",
  };

  // Upload the PDF file to S3
  s3.upload(params, async (err, data) => {
    if (err) {
      console.error("Error uploading file:", err);
      res.status(500).send({
        success: false,
        message: "Could not upload file",
        error: err,
      });
    } else {
      console.log("File uploaded successfully:", data.Location);

      //set the pdfUrl field with this newly uploaded url
      //save the changes
      assessmentPdfToken.pdfUrl = data.Location;
      await assessmentPdfToken.save();

      res.status(201).send({
        success: true,
        message: "File uploaded successfully",
      });
    }
  });
});

function isValidObjectId(id) {
  if (ObjectId.isValid(id)) {
    if (String(new ObjectId(id)) === id) return true;
    return false;
  }
  return false;
}

router.get("/periodic", async function (req, res) {
  const { user, assessmentVersion } = req.query;
  if (!user || !assessmentVersion || !isValidObjectId(user)) {
    res.status(400).send({
      success: false,
      error: "invalid args",
    });
    return;
  }

  const answers = await Answer.find({
    user,
    // partialAssessmentVersion: assessmentVersion,
  })
    .populate("qId")
    .catch((e) => {
      if (e) {
        console.log(e);
        res.status(500).send({
          success: false,
          error: e,
        });
        return;
      }
    });

  var answerMap = answers.reduce((acc, cur) => {
    if (acc[cur.qnId]) acc[cur.qnId].push(cur);
    else acc[cur.qnId] = [cur];

    return acc;
  }, {});

  // console.log({ answerMap });
  // console.log(answerMap["5fd7f1e0f2a3af00131bfba3"]);

  // need to format the question,answer and given answer
  // so it becomes easily readable

  res.status(200).send({
    success: true,
    data,
  });
});

router.post("/get-blob", upload.single("file"), async function (req, res) {
  const file = req.file;

  const blobData = file.buffer;

  console.log("Received file:", file);

  res.status(200).send({
    success: true,
    fileBuffer: blobData,
  });
});

module.exports = router;
