const mongoose = require("mongoose");

const Organisation = mongoose.model("Organisation", {
  name: { type: String, required: true },
  type: {
    type: String,
    required: true,
    enum: ["SU", "PROVIDER", "GLOBAL"],
  },
  logo: { type: String },
});

module.exports = Organisation;
