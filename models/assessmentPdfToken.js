const mongoose = require("mongoose");

const AssessmentPdfToken = mongoose.model("AssessmentPdfToken", {
  token: { type: String, required: true },
  email: { type: String, required: true },
  assessmentVersion: { type: Number, required: true },
  createdAt: { type: Date, default: Date.now() },
  pdfUrl: { type: String },
});

module.exports = AssessmentPdfToken;
