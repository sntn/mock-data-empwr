const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

const GlobalConfig = mongoose.model("GlobalConfig", {
  termsContent: { type: String, required: true, index: true },
  signupContent: { type: String, required: true },
  lastModifiedOn: { type: Date },
  questionnaire: { type: Schema.Types.ObjectId, ref: "Questionnaire" },
  qnList: [{ type: Schema.Types.ObjectId, ref: "Questionnaire" }],
  defaultRecommend: [
    { type: Schema.Types.ObjectId, ref: "MasterRecommendation" },
  ],
  maxAllottableArchetypes: { type: Number },
  mood: { type: Array },
  dietRestrictions: { type: Array },
  badges: { type: Array },
  rewardScore: { type: Number, default: 10 },
  rewardRedeem: { type: Array },
  casualReMonth: { type: Number },
  aiReMonth: { type: Number },
  level0: { type: Boolean, default: true },
  level1: { type: Boolean, default: true },
  level2: { type: Boolean, default: true },
  level3: { type: Boolean, default: true },
  level4: { type: Boolean, default: true },
  level5: { type: Boolean, default: true },
  // dietPlan: {
  //   breakfast: { type: Number },
  //   lunch: { type: Number },
  //   dinner: { type: Number },
  // },
  empowerScoreActivityThreshold: { type: Number },
  empowerScoreChangeRate: [
    {
      scoreLowerBound: { type: Number },
      scoreUpperBound: { type: Number },
      growthRate: { type: Number },
      declineRate: { type: Number },
    },
  ],
  empowerScoreIndexMultiplier: [
    {
      activityTrackedLowerBound: { type: Number },
      activityTrackedUpperBound: { type: Number },
      multiplier: { type: Number },
    },
  ],
  maxSupplementsCount: { type: Number },
  programId: { type: String, label: "Program ID for suggestic" },
  lastRecommendationUpdate: { type: Date },
  lastSupplementUpdate: { type: Date },
  badgeRewardNotification: {
    title: { type: String },
    body: { type: String },
    url: { type: String },
    deliveryTime: {
      hour: { type: Number },
      minute: { type: Number },
    },
  },
});

module.exports = GlobalConfig;
