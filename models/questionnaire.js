const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

const Questionnaire = mongoose.model("Questionnaire", {
  label: { type: String, required: true, index: true, unique: true },
  questions: [{ type: Schema.Types.ObjectId, ref: "Question" }],
  isMandatory: { type: Boolean, default: false },
});

module.exports = Questionnaire;
