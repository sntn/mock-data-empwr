const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  _id: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
  given_name: {
    type: String,
    required: true,
  },
  family_name: String,
  auth0Id: String,
  lastAssessmentDate: Date,
  organisation: {
    type: [mongoose.Types.ObjectId],
    required: true,
  },
  email: {
    type: String,
  },
});

userSchema.virtual("fullName").get(function () {
  return this.given_name + " " + this.family_name;
});

const User = mongoose.model("User", userSchema);

module.exports = User;
