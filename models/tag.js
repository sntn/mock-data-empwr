const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

const Tag = mongoose.model("Tag", {
  label: { type: String, required: true, index: true },
  fieldToMap: { type: String },
});

module.exports = Tag;
