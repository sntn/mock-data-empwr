const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

const Question = mongoose.model("Question", {
  description: { type: String },
  label: { type: String, required: true, index: true, unique: true },
  type: {
    type: String,
    enum: ["MULTISELECT", "SPECIAL", "MULTICHOICE"],
    required: true,
    index: true,
  },
  specialType: {
    type: String,
    enum: ["DATE", "TEXT", "NUMBER"],
  },
  weightage: { type: Number, required: true },
  choice: { type: Array },
  qnId: { type: Schema.Types.ObjectId, ref: "Questionnaire" },
  symptom: { type: Schema.Types.ObjectId, ref: "Symptoms" },
  tag: { type: Schema.Types.ObjectId, ref: "Tag" },
});

module.exports = Question;
