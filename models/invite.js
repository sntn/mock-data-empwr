const mongoose = require("mongoose");

const inviteSchema = new mongoose.Schema({
    _id: {
        type: mongoose.Types.ObjectId,
        required: true,
    },
    createdAt: {
        type: Date,
        required: true,
    },
    code: {
        type: String,
        required: true,
    },
    user: {
        type: mongoose.Types.ObjectId,
        required: true,
        ref: 'User',
    },
    organisation: {
        type: [mongoose.Types.ObjectId],
        required: true
    },
    active: {
        type: Boolean,
        required: true
    }
});



const Invite = mongoose.model('Invite', inviteSchema);

module.exports = Invite;