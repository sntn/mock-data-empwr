const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

const personnelSchema = new mongoose.Schema({
  email: { type: String },
  name: { type: String },
  auth0Id: { type: String },
  auth0Ids: [
    {
      orgType: { type: String, enum: ["SU", "PROVIDER"] },
      auth0Id: { type: String },
    },
  ],
  organisation: { type: Schema.Types.ObjectId, ref: "Organisation" },
});

const Personnel = mongoose.model("Personnel", personnelSchema);

module.exports = Personnel;
