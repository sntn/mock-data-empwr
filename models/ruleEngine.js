const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

const Ruleengine = mongoose.model("Ruleengine", {
  rule: { type: String, required: true },
  cells: { type: String, required: true },
  active: { type: Boolean, default: false },
  tree: [
    {
      questionnaire: { type: Schema.Types.ObjectId, ref: "Questionnaire" },
      label: String,
      type: { type: Number, enum: [0, 1] },
      branch: Number,
      nextIdx: Number,
      options: [String],
      position: Number,
    },
  ],
});

module.exports = Ruleengine;
