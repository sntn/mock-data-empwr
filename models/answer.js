const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

const Answer = mongoose.model("Answer", {
  assessmentVersion: { type: Number, default: 1 },
  qnId: { type: Schema.Types.ObjectId, ref: "Questionnaire", required: true },
  qId: { type: Schema.Types.ObjectId, ref: "Question", required: true },
  options: { type: Array },
  description: { type: String },
  weight: { type: Number, required: true },
  user: { type: Schema.Types.ObjectId, ref: "User", required: true },
  // weight: { type: Number },
  score: { type: Number, required: true },
  maxScore: { type: Number, required: true },
  symptom: { type: Schema.Types.ObjectId, ref: "Symptoms" },
  severity: { type: String },
  createdAt: { type: Date },
  tag: { type: Schema.Types.ObjectId, ref: "Tag" },
});

module.exports = Answer;
