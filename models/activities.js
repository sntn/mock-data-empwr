const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const activitySchema = new Schema({
  userId: { type: Schema.Types.ObjectId, ref: "User", required: true },
  logDate: { type: Date },
  duration: { type: Number },
  activityType: { type: String },
  calories: { type: Number },
  description: { type: String },
  distance: { type: Number },
  steps: { type: Number },
  device: { type: String },
  meta: { type: Array },
});

module.exports = mongoose.model("useractivity", activitySchema);
