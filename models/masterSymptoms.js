const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

const Symptoms = mongoose.model("Symptoms", {
  name: { type: String, required: true, index: true },
  weightage: { type: Number },
  scoreLookup: {
    None: { type: Number },
    Mild: { type: Number },
    Med: { type: Number },
    Severe: { type: Number },
  },
});

module.exports = Symptoms;
