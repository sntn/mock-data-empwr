const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

const Activity = mongoose.model("Activity", {
  mood: { type: String, index: true },
  activityDate: { type: Date, default: new Date() },
  user: { type: Schema.Types.ObjectId, required: true, ref: "User" },
  sleep: {
    hours: { type: Number },
    isComplete: { type: Boolean },
  },
  exercise: {
    type: Array,
  },
  meditation: {
    hours: { type: Number },
    isComplete: { type: Boolean },
  },
  organisation: [{ type: Schema.Types.ObjectId, ref: "Organisation" }],
});

module.exports = Activity;
