const NodeCache = require( "node-cache" );

const cache = new NodeCache( { stdTTL: 86400, checkperiod: 120 } );

function getUserCacheData(auth0Id) {
    const data = cache.get(`user__${auth0Id}`);
    return data;
}

function setUserCacheData(auth0Id, data) {
    const success = cache.set(`user__${auth0Id}`, data);
    return success;
}

module.exports = {getUserCacheData, setUserCacheData};