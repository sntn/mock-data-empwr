const Activity = require("../models/activity");
const Personnel = require("../models/Personnel");
const User = require("../models/user");
const Organisation = require("../models/Organisation");
const moment = require("moment");

const activityData = require("../data/activityMockMap.json");
const users = require("../data/userMock.json");

const userDetails = async (/* auth0Id */) => {
  // const personnel = await Personnel.findOne({
  //   $or: [{ auth0Id }, { auth0Ids: { $elemMatch: { auth0Id } } }],
  // }).populate("organisation");

  let formattedResp = {
    email: "testUser@example.com",
    name: "Test User :)",
    auth0Id: "sampleAuth0id",
    organisation: "Sample Organization",
    organisationId: "sampleOrganizationId",
    logo: null,
  };

  return formattedResp;
};

const userSearchData = async (/* orgId */) => {
  // const userList = await User.find({ organisation: orgId })
  //   .select("email given_name family_name")
  //   .sort({ given_name: 1 });

  let formattedResp = users.map((user) => {
    let obj = {
      email: user.email,
      name: user.name,
    };

    return obj;
  });

  return formattedResp;
};

const numberToMinutes = (hours) => {
  if (!hours) return null;
  const minutes = hours * 60;
  return parseFloat(minutes.toFixed(3));
};

const getUserActivities = async (email, type, days, startDate, endDate) => {
  //getting the user
  // const user = await User.findOne({ email });

  // if (!user) {
  //   return "no user found";
  // }

  // console.log("found user: ", user._id);

  //creating the db query based on api queries
  // let query = { user: user._id };
  // if (type === "days") {
  //   let start = new Date();
  //   start.setDate(start.getDate() - parseInt(days));
  //   query.activityDate = { $gte: start };
  // } else if (type === "range") {
  //   let start = moment(startDate).format("YYYY-MM-DD");
  //   let end = moment(endDate).format("YYYY-MM-DD");
  //   query.activityDate = { $gte: start, $lt: end };
  // }

  let activities = [];
  if (type === "days") {
    for (var i = 0; i < days; i++) {
      let date = moment().subtract(i, "days").format("YYYY-MM-DD");
      activities = [
        ...activities,
        ...activityData[date].filter((activity) => activity.email == email),
      ];
    }
  } else {
    let current = moment(startDate);
    let end = moment(endDate).add(1,"days");
    while (current.format("YYYY-MM-DD") != end.format("YYYY-MM-DD")) {
      activities = [...activities, ...activityData[current.format("YYYY-MM-DD")].filter((activity) => activity.email == email)]
      current = current.add(1, "days")
      //console.log(current.format("YYYY-MM-DD"), end.format("YYYY-MM-DD"));
    }
  }

  // console.log(query);

  //getting all activities of the user
  // activities = await Activity.find(query)
  //   .select("activityDate sleep exercise meditation")
  //   .sort({ activityDate: 1 });

  //need to fill the dates where there was no activities,
  //to show the chart properly in frontend
  let fillFrom;

  //we need to subtract one more day from starting date, because the data may start from
  //a single null data point, in that case the date difference wont be > 1,
  //so the first iteration nullfilling will not work
  if (type === "days")
    fillFrom = moment()
      .subtract(parseInt(days) + 1, "days")
      .format("DD/MM/YYYY");
  else fillFrom = moment(startDate).subtract(1, "day").format("DD/MM/YYYY");

  let lastActivityDate = fillFrom,
    sleepActivities = [],
    meditationActivities = [],
    exerciseActivities = [];

  activities.map((activity) => {
    const { activityDate, sleep, exercise, meditation } = activity;
    let formattedDate = moment(activityDate).format("DD/MM/YYYY");

    //filling intermediate datapoints from last activity date to yesterday
    // let d1 = moment(lastActivityDate, "DD/MM/YYYY");
    // let d2 = moment(activityDate);
    // let diff = d2.diff(d1, "days");
    // for (let i = 0; i < diff - 1; i++) {
    //   const date = d1.add(1, "day").format("DD/MM/YYYY");
    //   // console.log(date);
    //   let obj = { date, hours: null };
    //   sleepActivities.push(obj);
    //   meditationActivities.push(obj);
    //   exerciseActivities.push(obj);
    // }
    // console.log(">", formattedDate);
    lastActivityDate = moment(activityDate).format("DD/MM/YYYY");

    sleepActivities.push({
      date: formattedDate,
      minutes: sleep.hours ? numberToMinutes(sleep.hours) : null,
    });
    meditationActivities.push({
      date: formattedDate,
      minutes: meditation.hours ? numberToMinutes(meditation.hours) : null,
    });
    exerciseActivities.push({
      date: formattedDate,
      minutes: exercise.hours ? numberToMinutes(exercise.hours) : null,
    });
  });

  //we need to repeat the process from the very last activity date
  //till today's date or the range end date one last time
  // let d1 = moment(lastActivityDate, "DD/MM/YYYY");
  // let d2;
  // if (type === "days") {
  //   d2 = moment();
  // } else {
  //   d2 = moment(endDate).add(1, "day");
  // }
  // let diff = d2.diff(d1, "days");

  // for (let i = 0; i < diff - 1; i++) {
  //   let date = d1.add(1, "day").format("DD/MM/YYYY");
  //   // console.log(date);
  //   let obj = { date, minutes: null };
  //   sleepActivities.push(obj);
  //   meditationActivities.push(obj);
  //   exerciseActivities.push(obj);
  // }

  return { sleepActivities, meditationActivities, exerciseActivities };
};

module.exports = { userDetails, userSearchData, getUserActivities };
