const Activity = require("../models/activity");
const User = require("../models/user");
const momentz = require("moment-timezone");

const activityData = require("../data/activityMockMap.json");
const users = require("../data/userMock.json");

const addUserActivities = (activities, userId, obj, type) => {
  let today = new Date();
  let sleepCount = 0;
  let totalSleep = 0;
  let moodCount = 0;
  let totalMood = 0;
  let lastActivityDate = null;

  for (let i = 0; i < activities.length; i++) {
    if (activities[i]["_id"] == (userId)) {
      // console.log(
      //   "\nfound activity on ",
      //   activities[i].activityDate,
      //   activities[i]
      // );

      if (!lastActivityDate || activities[i].activityDate > lastActivityDate)
        lastActivityDate = new Date(activities[i].activityDate);

      // getting total, min, max sleep
      if (activities[i].sleep && activities[i].sleep.hours) {
        sleepCount += 1;
        totalSleep += activities[i].sleep.hours;

        if (obj.minSleep === "_" || activities[i].sleep.hours < obj.minSleep)
          obj.minSleep = activities[i].sleep.hours;
        if (obj.maxSleep === "_" || activities[i].sleep.hours > obj.maxSleep)
          obj.maxSleep = activities[i].sleep.hours;
      }

      //getting total, min, max mood
      if (activities[i].mood) {
        moodCount += 1;
        let moodNo;
        if (activities[i].mood === "Great") moodNo = 5;
        if (activities[i].mood === "Good") moodNo = 4;
        if (activities[i].mood === "OK") moodNo = 3;
        if (activities[i].mood === "Bad") moodNo = 2;
        if (activities[i].mood === "Terrible") moodNo = 1;

        totalMood += moodNo;

        if (obj.minMood === "_" || obj.minMood > moodNo) obj.minMood = moodNo;
        if (obj.maxMood === "_" || obj.maxMood < moodNo) obj.maxMood = moodNo;
      }
    }
  }

  //adding average values and inactive days
  if (sleepCount > 0) {
    obj.sleepCount = sleepCount;
    if (
      (type == "weekly" && sleepCount < 4) ||
      (type == "monthly" && sleepCount < 20)
    )
      obj.averageSleep = "ND";
    else obj.averageSleep = parseFloat((totalSleep / sleepCount).toFixed(2));
  }
  if (moodCount > 0) {
    obj.moodCount = moodCount;
    if (
      (type == "weekly" && moodCount < 4) ||
      (type == "monthly" && moodCount < 20)
    )
      obj.averageMood = "ND";
    else obj.averageMood = parseFloat((totalMood / moodCount).toFixed(2));
  }
  if (lastActivityDate) {
    const oneDay = 24 * 60 * 60 * 1000; // one day in milliseconds
    const diffDays = Math.round(Math.abs((today - lastActivityDate) / oneDay));
    obj.inactivity = diffDays;
  }

  return obj;
};

const dailySummary = async (orgId) => {
  var today = new Date();
  // var yesterday = new Date(today.getTime() - 24 * 60 * 60 * 1000);
  const threeDayAgoDate = new Date();
  threeDayAgoDate.setDate(threeDayAgoDate.getDate() - 4);

  // const activities = await Activity.find({
  //   organisation: orgId,
  //   activityDate: { $gte: yesterday, $lt: today },
  // }).select("user mood activityDate sleep exercise meditation");

  const activities = activityData[momentz().format("YYYY-MM-DD")];
  // const users = await User.find({ organisation: orgId })
  //   .select("_id email given_name family_name")
  //   .sort({ given_name: 1 });

  let response = [];

  // console.log({ activities });

  activities.map((activity, index) => {
    let obj = {
      user: activity["_id"],
      name: activity["name"],
      email: activity["email"],
      mood: "_",
      sleepScore: "_",
      exerciseScore: "_",
      meditationScore: "_",
      date: null,
      inactivity: null,
      moodStatus1: 0,
      moodStatus2: 0,
    };

    // console.log("\nfound activity of: ", given_name);

    obj.date = activity.activityDate;

    const lastActivityDate = new Date(activity.activityDate);
    const oneDay = 24 * 60 * 60 * 1000; // one day in milliseconds
    const diffDays = Math.ceil(Math.abs((today - lastActivityDate) / oneDay));
    obj.inactivity = diffDays;

    if (activity.mood) {
      if (activity.mood === "Great") {
        obj.mood = activity.mood;
        obj.moodStatus1 += 1;
        if (lastActivityDate > threeDayAgoDate) {
          obj.moodStatus2 += 1;
        }
      }
      if (activity.mood === "Good") {
        obj.mood = activity.mood;
        obj.moodStatus1 += 1;
        if (lastActivityDate > threeDayAgoDate) {
          obj.moodStatus2 += 1;
        }
      }
      if (activity.mood === "OK") {
        obj.mood = activity.mood;
        obj.moodStatus1 += 1;
        if (lastActivityDate > threeDayAgoDate) {
          obj.moodStatus2 += 1;
        }
      }
      if (activity.mood === "Bad") obj.mood = activity.mood;
      if (activity.mood === "Terrible") obj.mood = activity.mood;
    }

    if (activity.sleep && activity.sleep.hours)
      obj.sleepScore = activity.sleep.hours;

    if (activity.exercise && activity.exercise.hours) {
      obj.exerciseScore = activity.exercise.hours;
    }

    if (activity.meditation && activity.meditation.hours)
      obj.meditationScore = activity.meditation.hours;

    //add code to update moodStatus2

    response.push(obj);
  });

  return response;
};

const weeklySummary = async (orgId) => {
  let activities = [];
  for (var i = 0; i < 7; i++) {
    let date = momentz().subtract(i, "days").format("YYYY-MM-DD");
    activities = [...activities, ...activityData[date]];
  }
  // const lastWeek = new Date();
  // lastWeek.setDate(lastWeek.getDate() - 7);

  // const activities = await Activity.find({
  //   organisation: { $in: [orgId] },
  //   activityDate: { $gte: lastWeek },
  // }).select("user mood activityDate sleep");

  // const users = await User.find({ organisation: { $in: [orgId] } })
  //   .select("_id email given_name family_name")
  //   .sort({ given_name: 1 });

  let response = [];

  // console.log({ activities });

  users.map(({ _id, email, name }) => {
    let obj = {
      user: _id,
      name: name,
      email,
      inactivity: null,
      minSleep: "_",
      maxSleep: "_",
      sleepCount: 0,
      averageSleep: "ND",
      minMood: "_",
      maxMood: "_",
      moodCount: 0,
      averageMood: "ND",
      inactivity: null,
    };

    obj = addUserActivities(activities, _id, obj, "weekly");

    response.push(obj);
  });

  return response;
};

const monthlySummary = async (orgId) => {
  let activities = [];
  for (var i = 0; i < 30; i++) {
    let date = momentz().subtract(i, "days").format("YYYY-MM-DD");
    activities = [...activities, ...activityData[date]];
  }
  // var today = new Date();
  // const lastMonth = new Date();
  // lastMonth.setDate(lastMonth.getDate() - 30);

  // const activities = await Activity.find({
  //   organisation: { $in: [orgId] },
  //   activityDate: { $gte: lastMonth },
  // }).select("user mood activityDate sleep");

  // const users = await User.find({ organisation: { $in: [orgId] } })
  //   .select("_id email given_name family_name")
  //   .sort({ given_name: 1 });

  let response = [];

  // console.log({ activities });

  users.map(({ _id, email, name }) => {
    let obj = {
      user: _id,
      name: name,
      email,
      inactivity: null,
      minSleep: "_",
      maxSleep: "_",
      sleepCount: 0,
      averageSleep: "ND",
      minMood: "_",
      maxMood: "_",
      moodCount: 0,
      averageMood: "ND",
      inactivity: null,
    };

    obj = addUserActivities(activities, _id, obj, "monthly");

    response.push(obj);
  });

  return response;
};

module.exports = { dailySummary, weeklySummary, monthlySummary };
