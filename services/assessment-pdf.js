const ObjectId = require("mongoose").Types.ObjectId;

const ruleEngineConfig = require("../active-rule-engine.json");

const formatInPages = (allQuestionsData) => {
  var pages = [];
  var weight = 0;

  const getQuestionWeight = (question) => {
    //if question type is special then instead of counting choices, add given answer
    let weight = 1.75; //for question
    if (question.type === "SPECIAL") weight += 1;
    else weight += question.choices.length + 1;
    // console.log("weight: ", weight);
    return weight;
  };

  const headerScore = 2.86;

  const allQuestions = allQuestionsData;

  let page = allQuestions.reduce((acc, current) => {
    let headerWithFirstQuestionWeight =
      headerScore + getQuestionWeight(current.questions[0]);
    // console.log(
    //   current.questionnaire,
    //   "header  + 1st qn weight: ",
    //   headerWithFirstQuestionWeight +
    //     " total score: " +
    //     (weight + headerWithFirstQuestionWeight)
    // );

    if (weight + headerWithFirstQuestionWeight > 40) {
      pages.push(acc);
      acc = [];
      weight = 0;
    }
    acc.push({
      type: "HEADER",
      label: current.questionnaire,
    });
    weight += headerScore;
    current.questions.reduce((pageAcc, currentQuestion, index, questions) => {
      if (weight + getQuestionWeight(currentQuestion) > 40) {
        pages.push(pageAcc.slice(0, pageAcc.length));
        pageAcc.length = 0;
        // console.log(pageAcc);
        weight = 0;
      }
      pageAcc.push(currentQuestion);
      weight += getQuestionWeight(currentQuestion);
      // console.log(
      //   currentQuestion.question +
      //     " (" +
      //     currentQuestion.choices.length +
      //     " choices)" +
      //     " = " +
      //     weight
      // );
      // console.log(pageAcc);
      return pageAcc;
    }, acc);
    return acc;
  }, []);

  pages.push(page);
  // console.log(pages);

  return pages;
};

function isChoicePresent(options, choices) {
  for (const choice of choices) {
    if (options.includes(choice)) {
      return true;
    }
  }
  return false;
}

const ruleEngine2 = async (
  questionnaire,
  userId,
  allUserGivenAnswers,
  assessmentVersion
) => {
  // const ruleEngineConfig = await RuleEngineModel.findOne({ active: true });
  // console.log("getting rule engine");

  if (!ruleEngineConfig) {
    throw "No rule engine found";
  }

  const rules = JSON.parse(JSON.parse(ruleEngineConfig.rule));

  const _rulesOfQuestionnaire = rules[questionnaire.toString()];

  //if there are no options, and there is fallback, then the fallback will be returned
  if (!_rulesOfQuestionnaire.options.length && _rulesOfQuestionnaire.fallback) {
    return [_rulesOfQuestionnaire.fallback];
  }

  const appliedRules = _rulesOfQuestionnaire;
  const qnRules = appliedRules.options;

  let filteredRules = [];

  if (qnRules.length) {
    for (let i = 0; i < qnRules.length; i++) {
      const { metaData } = qnRules[i];
      const choiceArray = Array.isArray(metaData.choice)
        ? [...metaData.choice]
        : metaData.choice;

      const specificAnswerExist = allUserGivenAnswers.find(
        (ans) =>
          ans.assessmentVersion === parseInt(assessmentVersion) &&
          // ans.assessmentVersion === 2 &&
          ans.user.equals(ObjectId(userId)) &&
          ans.qnId.equals(ObjectId(metaData.qn)) &&
          ans.qId.equals(ObjectId(metaData.q)) &&
          isChoicePresent(ans.options, choiceArray)
      );

      if (specificAnswerExist) {
        filteredRules.push(qnRules[i]);
      }
    }
  }

  // console.log("filtered rules length: ", filteredRules.length);

  return filteredRules
    .map((e) => _rulesOfQuestionnaire[e.id])
    .filter((el, idx, arr) => arr.indexOf(el) == idx);
};

const traverse = (tree) => {
  var index = 0;
  var questionnaireList = [];
  var stack = [];
  while (true) {
    const node = tree[index];
    if (node.type === 0) {
      // console.log(node.label);
      questionnaireList.push(node.label);
      if (node.nextIdx) {
        index = node.nextIdx;
      } else if (stack.length > 0) {
        index = stack.pop();
      } else break;
    } else {
      if (node.branch) {
        if (node.nextIdx) {
          stack.push(node.nextIdx);
        }
        index = node.branch;
      } else {
        console.log("Connector should have a branch value");
        break;
      }
    }
  }
  // console.log("Total Assessment Length: " + questionnaireList.length);
  // console.log(questionnaireList);

  return questionnaireList;
};

module.exports = {
  ruleEngine2,
  formatInPages,
  traverse,
};
