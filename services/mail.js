const nodemailer = require("nodemailer");
const smtpTransport = require("nodemailer-smtp-transport");
const path = require("path");
const hbs = require("nodemailer-express-handlebars");

const transporter = nodemailer.createTransport(
  smtpTransport({
    service: "sendgrid",
    host: "smtp.sendgrid.net",
    auth: {
      user: process.env.EMAIL,
      pass: process.env.PASSWORD,
    },
  })
);
const transporterWithTemplate = nodemailer.createTransport(
  smtpTransport({
    service: "SendGrid",
    host: "smtp.sendgrid.net",
    auth: {
      user: process.env.EMAIL,
      pass: process.env.PASSWORD,
    },
  })
);

const sendEmailWithTemplate = async (
  emailId,
  subject = "Test mail",
  template,
  options,
  msg = undefined
) => {
  const handlebarOptions = {
    viewEngine: {
      extName: ".handlebars",
      partialsDir: path.resolve("./views/"),
      defaultLayout: false,
    },
    viewPath: path.resolve("./views/"),
    extName: ".handlebars",
  };

  const mailOptions = {
    from: process.env.EMAIL_FROM,
    to: emailId,
    subject: subject,
    template,
    context: options,
  };
  if (msg) mailOptions.text = msg;

  transporterWithTemplate.use("compile", hbs(handlebarOptions, options));
  transporterWithTemplate.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
      throw(error);
    } else {
      console.log("Email sent: " + info.response);
    }
  });
};

module.exports = {
  sendEmailWithTemplate,
};
